#### 命名空间

```typescript
namespace IteratorPatter {
    export interface Istat {} // 在命名空间里面，要使用export来暴露
    export class CConcrete implements Istat { // 在命名空间里面，类可以正常实现接口，同样的，暴露也要使用export
        constructor(){}
    }
}

# 同一个命名空间过大，需要分割文件，那么需要使用标签引用来标识，这样就可以像在同一个文件一样编写代码
/// <reference path="Validation.ts" />

# 命名空间外可以通过namespace.Ib访问里面的接口，前提是Ib是要被export出来，不然不能访问哟

# 思考上面的代码，如果是多重嵌套的命名空间，岂不是需要namespaceA.namespaceB.namespaceC。这样就产生了别名的这一概念，可以通过 import D = namespaceA.namespaceB.namespaceC;

# 当涉及到多文件时，我们必须确保所有编译后的代码都被加载了。
第一种方式，把所有的输入文件编译为一个输出文件，需要使用--outFile标记：
tsc --outFile sample.js Test.ts

# 注意一点就是对模块不需要使用命名空间
```



#### 模块解释 TS vs Node

```typescript
# 相对路径
文件./root/src/featrue1/c.ts里面有语句import { moduleA } from './moduleA';
- TS
 1. ./root/src/featrue1/moduleA.ts
 2. ./root/src/featrue1/moduleA.tsx
 3. ./root/src/featrue1/moduleA.d.ts
 4. ./root/src/featrue1/moduleA/package.json (如果指定了"types"属性)

- Node
 1. ./root/src/featrue1/moduleA.js
 2. ./root/src/featrue1/moduleA/package.json (里面有没有main字段,有的话根据main字段)
 3. ./root/src/featrue1/moduleA/index.js

# 非相对路径
文件./root/src/featrue1/c.ts里面有语句import { moduleA } from './moduleA';
- TS
 1. ./root/src/featrue1/moduleA.ts
 2. ./root/src/featrue1/moduleA.tsx
 3. ./root/src/featrue1/moduleA.d.ts
 4. ./root/src/featrue1/moduleA/package.json (如果指定了"types"属性)

 1. ./root/src/moduleA.ts
 2. ./root/src/moduleA.tsx
 3. ./root/src/moduleA.d.ts
 4. ./root/src/moduleA/package.json (如果指定了"types"属性)
- Node
1. ./root/src/featrue1/moduleA.js
2. ./root/src/featrue1/moduleA/package.json (里面有没有main字段,有的话根据main字段)
3. ./root/src/featrue1/moduleA/index.js

4. ./root/src/moduleA.js
5. ./root/src/moduleA/package.json (里面有没有main字段,有的话根据main字段)
6. ./root/src/moduleA/index.js

# 设置baseUrl来告诉编译器到哪里去查找模块。 所有非相对模块导入都会被当做相对于 baseUrl
baseUrl的值由以下两者之一决定：
 - 命令行中baseUrl的值（如果给定的路径是相对的，那么将相对于当前路径进行计算）
 - ‘tsconfig.json’里的baseUrl属性（如果给定的路径是相对的，那么将相对于‘tsconfig.json’路径进行计算）
```

