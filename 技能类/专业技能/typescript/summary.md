#### 数据类型

```typescript
boolean string number any void undefined null 

# 数组
list:number[]=[1, 2, 3, 4];

# 枚举
enum Weeaks{
    Mon,
    Tues,
    Wed
}

# 联合数据类型
data:number | string | boolean

# 类型推论
let x3 = 3; x3 = "hahah" // 报错 

# 类型断言
let strLength:number = (<string>x5).length;
let strLength:number = (x5 as string).length;

```



#### 接口

```typescript
# 定义接口 提取相同方法,约束和规范的作用
interface Iprinter {
    Printing(msg:string):string;
}

interface Imessage {
    getMsg():string;
}

# 实现接口/对类的约束
class colorPrinter implements Iprinter,Imessage {
    Printing(msg:string):string {
        return `打印${ string }成功`;
    }
    getMsg():string {
        return '实现多个接口成功';
    }
}

let p1 = new colorPrinter();
console.log(p1.Printing('简历'))

# 对函数的约束
interface ImyFunction {
    (a:string, b:number):boolean;
}
let fun1:ImyFunction;
fun1 = function(a:string, b:number):boolean {
    return false;
}

# 对数组的约束
interface IstuArr {
    [index:number]:string;
}
let arr1:Istuarr;
arr1=["aaa", "bbb"];

# 对JSON的约束
interface Idata {
    name:string,
    age?:number, // 可选参数
    readonly sex:string // 只读属性
}
function showData(n:Idata) {
    console.log(JSON.stringify(n));
}
showData({name: '你好', age: 19});
```



#### 类

```typescript
# 类的定义
class Person {
    name:string;
    age:number;
    constructor(name:string, age:number) {
        this.name = name;
        this.age = age;
    }
    print () {
        return `${ this.name }:${ this.age }`;
    }
}
let p = new Person('你好', 20);
console.log(p.print());

# 类的继承
class Student extends Person {
    number: string;
    school: string;
    constructor(number:string, school: string) {
        super('nihao', 100);
        this.number = number;
        this.school = school;
    }
    homework() {
        return `${ this.name }今年${ this.age }岁，就读于${ this.school }编号${ this.number }`
    }
}
let stu1 = new Student('1001', '北京大学');
// stu1.number = '1001';
// stu1.school = '北京大学';
console.log(stu1.homework());

# 接口的继承
interface Printer {
    getMsg();
}
interface ColorPrinter extends Printer {
    printing();
}
class HPPrinter implements ColorPrinter {
    getMsg () {}
    printing() {}
}

# 访问修饰符 public private protected 默认是public
class Person {
    age:number;
    private name:string;
    protected email: string;
}
// 访问修饰符
// public    均可访问
// private   子类不可访问
// protected 子类可访问

# 静态属性和静态方法
class Person {
    static name:string;
    static speak () {}
}

# 多态 
类似接口，约束子类，不同子类有不同的实现

# 抽象类和抽象方法
abstract class Animal {
    abstract eat();
    run () {
        
    }
}

```



#### 函数

```typescript
# 函数定义
function add(x, y):number {
    return x + y;
}

# 匿名函数
let add1 = function(x, y):number {
    return x + y;
}

# 函数的参数
function show(name, age?:number):void {
    console.log('无返回值的函数');
} // 可选参数一般放在参数的末尾

// 默认参数
function show(name, age = 0):void {
    console.log(age);
}

// 剩余参数
function add(x1, x2, ...x:number[]):number {
    return x1 + x2 + x.reduce((pre, cur) => pre+=cur,0)
}

# 函数的重载
function getInfo(name:string):void;
function getInfo(age:number):void;
function getInfo(str:any):void {}
```



#### 泛型

```typescript
# 泛型函数
function printarr1<T>(arr:T[]):void {
    ...
}
    
printarr1<number>([11, 22, 33])
printarr1<string>(['fsadfsa', 'fsdfdsa', 33])
    
# 泛型类
class myArrayList<T> {
    public name:T;
    list:T[]=[];
    add(val:T):void {
        
    }
}
let arr = new myArrayList<number>();

# 泛型接口
interface Iadd<T> {
    (x:T, y:T):T;
}
let add:Iadd<number>;
add = function(x:number, y:number) {
    return x + y;
}
```







