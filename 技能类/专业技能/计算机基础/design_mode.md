#### 设计模式

> 一些经过验证，对某种场景具有最佳实践的设计，一般代码效益被认为是最优解，一般会处理三种情景。
>
> - 创造型设计模式，负责解决某种场景下对对象创建的要求，常见的有构造器模式，工厂模式、抽象工厂模式、原型模式以及建造者模式
> - 结构设计模式，有助于在系统的某一部分发生改变，整个系统不需要改变，该某事有助于对系统中某部分没有达到某一目的的部分进行重组，长江的有：装饰模式、外观模式、享元模式，适配器模式何代理模式。
> - 行为设计模式，关注改善或精简在系统中不同对象间通信，常见有迭代模式，中介者模式、观察者模式和访问者模式

#### 观察者模式

```javascript
# 被观察者维护一组观察者对象。当有新的信息到来的时候，被观察者通过广播的方式告诉已经在注册的观察者。

收益：保持对象之间的信息一致性，而且观察者随时可以脱离被观察只要它愿意，当然随时也可以接受被观察的广播的信息，只要它已经注册

所以，观察者需要实现两个接口：
 - 增加观察者接口
 - 删除观察者接口

被观察者需要实现一个接口
 - 用于更新数据的接口

class ObserverList {
    constructor() {
        this.clienList = [];
    }
    
    add (obj, index) {
        // 我没有使用引用，也没有记录最近一次添加的索引
        this.clientList.push(str);
        let pointer  = -1;
        
        if (index === 0) {
            this.clientList.unshift(obj);
            pointer = index;
        } else if (index === this.clientList.length) {
            this.clientList.push(obj);
            pointer = index;
        }
        return pointer;
    }
    
    getIndex (obj, startIndex) {
        let i = startIndex,
            pointer = -1;
        
        while (i < this.clientList.length) {
            if(this.clientList[i] === obj) {
                pointer = i;
            }
            i++;
        }
        return pointer;
    }
    
    removeAt (index) {
        if (index === 0) {
            this.clientList.shift();
        } else if (index === this.clientList.length - 1) {
            this.clientList.pop();
        }
    }
    
    reset () {
        this.clientList = []；
    }
    
    count () {
        return this.clientList.length;
    }
    
    get (index) {
        if(index > -1 && index < this.clientList.length) {
            return this.clientList[index];
        }
    }
    
}

function extend (extension, obj) {
    for (let key in extension) {
        obj[key] = extension[key];
    }
}

class Subject () {
    constructor() {
        this.observers = new ObserverList();
    }
    
    addObserver (observer) {
        this.observers.add(observer);
    }
    
    removerObserver (observer) {
        this.observers.removeAt(this.observers.getIndex(observer, 0))
    }
    
    notify (context) {
        let observerCount = this.observers.count();
        for (let i = 0 ; i < oberserverCount; i++) {
            this.observers.get(i).update(context);
        }
    }
    
}

var controlCheckbox = document.getElementById( "mainCheckbox" ),
04 addBtn = document.getElementById( "addNewObserver" ),
05 container = document.getElementById( "observersContainer" );
06
07
08 // 具体的被观察者
09
10 //Subject 类扩展controlCheckbox 类
11 extend( new Subject(), controlCheckbox );
12
13 //点击checkbox 将会触发对观察者的通知
14 controlCheckbox["onclick"] = new Function( "controlCheckbox.Notify(controlCheckbox.checked)" );
15
16
17 addBtn["onclick"] = AddNewObserver;
18
19 // 具体的观察者
20
21 function AddNewObserver(){
22
23 //建立一个新的用于增加的checkbox
24 var check = document.createElement( "input" );
25 check.type = "checkbox";
26
27 // 使用Observer 类扩展checkbox
28 extend( new Observer(), check );
29
30 // 使用定制的Update函数重载
31 check.Update = function( value ){
32 this.checked = value;
33 };
34
35 // 增加新的观察者到我们主要的被观察者的观察者列表中
36 controlCheckbox.AddObserver( check );
37
38 // 将元素添加到容器的最后
39 container.appendChild( check );

```

