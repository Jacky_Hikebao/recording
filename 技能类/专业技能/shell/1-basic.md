```javascript
# 表示注释的意思
#! eg:'#!/usr/bin/env bash' 结合一起时候，后面一般接着路径，然后就是程序。表示脚本用什么程序执行，第一行指令在不明确声明什么程序去执行脚本的时候生效
if 语句的结束标签是fi
${} 获取变量的意思
$() 执行命令，获取结果
$0 获取执行脚本的第一个参数 eg: ssh **.sh param1 param2 其中$1表示param2
chmod -/+x 其中x表示exe可执行权限的意思，-/+表示添加或者删除权限 chmod 777 xxx
ln -s x y // 第一个字母不是1，而是小写的L,第二个参数-s表示software，命令表示软连接x到y，访问y就是访问x,不加-s就是硬链接
exit 1 大于0表示异常退出
$? -ne 0  // 其中 $?表示上一条命令退出的状态、 -ne表示not equal 不等于的意思
```



#### shell里面一些奇淫技巧

```shell
# if里面的判断语句如果是取反的话，感叹号后面必须有空格
if [ ! true ]; then
	# 这里写需要执行的东西，而且要注意的是结束的时候要考虑要不要使用
	# exit 1
fi

# 使用$0时候注意事项
echo $0 
上面的shell命令，如果执行脚本的时候是使用 ./test.sh，那么输出的结果就是./test.sh

# 在shell中，会有以下语句用于获取当前的路径：
CURRENT_DIR="$(cd "$(dirname "$0")"; pwd)"
这里有一个技巧点，$0是执行当前脚本的路径 + 可执行路径，而dirname则是获取执行脚本的路径
以上命令 $(dirname "$0") 将返回该文件当前的上一层路径，通过cd到该路径，通过pwd获得当前路径，并保存到CURRENT_DIR中；

# mkdir -p参数是能直接创建一个不存在的目录下的子目录
一行上执行多个命令 ;
一行上执行多个命令且有逻辑判断 && ||

mkdir -p filename // 递归创建
mkdir filename //不递归

ll filename &>/dev/null || mkdir -p filename // 如果文件不存在则创建

# 创建环境变量路径
OLD_PATH=$PATH # 保留旧的环境变量,以便环境恢复
NEW="${CURRENT_PWD}/x/y/:${PATH}"

# 添加路径至环境中
export PATH=${NEW}

# 在shell里面嵌入其他解释型语言
/usr/bin/bash <<-EOF
// 这里编写其他脚本的语句
EOF

# 如何将多行命令输入到某一个文件里面去
cat <<-EOF > file
// 这里填写需要输入的内容
EOF

# 如何在脚本里面使用函数库,在脚本里面执行其它shell脚本即可
. ./xxx/xx.sh
source ./xx/xx.sh

function fn {
	# 这里写需要执行的代码
}

# 如何创建并使用函数
if [ ! -d xxx ]; then
	fn  # 这里执行fn
fi

# > & >>
- 在`linux`操作系统中，可以使用命令`echo 'xxxyyy' > test.txt`将'xxxyyy'写入文件`test.txt`,如果test.txt不存在则重新创建一个，如果存在则将里面的内容全部覆盖掉，注意，这里不是追加
- 如果在上面的基础上再加多一个大于号则代表追加`echo 'xxxyyy' >> test.txt`

# echo 颜色提示
echo -e "\e[1;31mThis is a red text.\e[0m"

# 如何分割字符串
awk '{BEGIN RS=":"}{print $0}'
```



#### shell命令

```shell
if
case
for while
until
break
continue
exit
shift
array
function

# 查询shell配置文件
rpm -rc bash

# su - root 区别 su root
/etc/profile // 系统 		加中横线执行 不加不执行
/etc/bashrc  // 系统 		加中横线执行 不加也执行
~/.bash_profile // 用户	加中横线执行 不加不执行
~/.bashrc   // 用户		加中横线执行 不加也执行
~/.bash_logout // 离开shell执行		
~/.bash_history // 离开shell执行	

# :set number ===显示行号==> 缩写:set nu

# shell可以使用命令行和脚本上面进行运行

# !num 执行哪条命令
# !string 找到最近一个string开头的命令并执行
# !$ 上一个命令的最后一个参数
# !! 上一个命令
# ^R
# alias 查看命名 unalias取消命名
定义临时别名
alias x='date'

# 定义永久别名vim .bashrc
# 执行shell可以用用 source *.sh
# 将X程序复制到y程序 cat x >>  y
```



#### 常用比较好用的指令

```shell
# ctrl类
+ u/k 向左向右删除
+ a/e 光标移到最左或者最右
+ y 撤回
```



#### 了解Java、C 、Shell、Python、Perl

```shell
Java 编译型语言 字节码（java虚拟机jdk） tomcat
C	编译型语言  二进制机器码
shell	解释型语言 (/usr/bin/shell)
perl	解释型语言 (/usr/bin/perl)
expect	解释型语言 (/usr/bin/expect)
python	解释型语言 (/usr/bin/python)
		编码 	字节码（Python虚拟机）

程序是由什么组成的？ ： 逻辑+数据
```



#### 执行命令的秘密

```shell
当使用bash bash.sh或者./shell.sh的时候
其实命令不是在当前的终端执行的，而是在sub shell里面执行的。对当前的shell不影响
使用 . bash.sh或者source bash.sh执行的话就是在当前的终端执行，立即生效
```



#### 退出终端后删除子进程

```shell
退出终端删除子进程，只要在命令后面加上&即可。eg：sleep 500 &
ctrl+z保存现场，fg回到现场
```



#### 管道 | tee

```shell
ip addr | grep 'ens33' // 过滤
ip addr | tee fileName // 结合管道，将输出作为文件的入口，然后得出结果并生成文件
echo '444' > fileName // 将444输入到某个文件，但是没有结果
```



#### 压缩文件命令

```shell
压缩：tar -zf dist.tgz dist
解压：tar -xf dist.tgz
	 unzip dist.zip
	 
-z 压缩
-x 解压
-f 要压缩|解压的文件，只能放在最后，而且后面加文件
-v 显示所有过程
```

#### 位置变量

```javascript
$num  // 位置参数
$?    // 上一个命令的返回值
$!    // 上一个后台进程的pid
$$    // 当前进程的pid
$#    // 参数个数
$@
$*
basename 始终最后一个名字 // 1/2/3 返回3
dirname 始终返回路径 // 1/2/3 返回1/2
```

#### 赋值

```javascript
# 显示赋值 
`` // 反引号可以使用$()代替 表示里面的命令会先被执行
"$var1"
var1=666


读取用户从键盘输入的值
read ip
read -n 2 name // 获取前两个字符
read -p "请输入姓名: " name // 单个赋值
read -p "请输入姓名，性别，年龄[name sex 20]: " name sex age // 多个赋值
```



#### 主机之间的拷贝命令SCP

```javascript
scp 是安全拷贝协议（Secure Copy Protocol）的缩写，和众多 Linux/Unix 使用者所熟知的拷贝（cp）命令一样。scp 的使用方式类似于 cp 命令，cp 命令将一个文件或文件夹从本地操作系统的一个位置（源）拷贝到目标位置（目的），而 scp 用来将文件或文件夹从网络上的一个主机拷贝到另一个主机当中去。
而加上参数 -r 表示递归拷贝整个目录

`ssh -p22 userName@remoteIP`: -p22表示端口
`scp -r dist/  dockernode-1:~`scp是 secure copy的缩写, scp是linux系统下基于ssh登陆进行安全的远程文件拷贝命令。dockernode-1是节点，分号后面的波浪号是路径，~是/root/的缩写,-r： 递归复制整个目录。
```

#### 避免重复输入sudo

```javascript
`sudo passwd root`:重新设置su的登陆密码,这样就不用每次command进行sudo了
```

#### gawk

```shell
gawk程序是Unix中原awk程序的GNU版本。现在我们平常使用的awk其实就是gawk，可以看一下awk命令存放位置，awk建立一个软连接指向gawk，所以在系统中你使用awk或是gawk都一样的。

free | sed -n '2p' | gawk 'x = int(( $3 / $2 ) * 100 ) {print x}' | sed 's/$/%/'
```

#### 输出多行到指定的文件里面

```shell
cat <<-EOF >> webpack.config.js
xxx
xx
x
EOF
```

#### kill

```javascript
kill 3 
表示给pid为3的发送信号15

kill %3
表示给作业号为3的进程发送信号
```



##### 获取变量长度：`${#xxx}`

##### 改变shell脚本执行方式有三种

- exit 退出整个程序
- break 退出循环程序
- continue 退出本次循环

##### shell脚本循环方式有三种

```shell
# 第一种--适用于不固定次数以及文件读取数据的情况
while read xxx
do
	{
	
	}&
done <xxx.text
wait

# 第二种--适用于固定次数的情况
for xx in {1..num}
do
	{
	
	}&
done
wait

# 第三种--适用于不固定次数，取值为反的循环情况
util false
do
	{
	
	}&
done
wait
```

##### 通过ssh远程执行命令的方式：`ssh xxx "xxx"`

##### 判断一个命令是否存在：`command -v xxx`或者`type xxx`

##### 给变量赋默认值: `${xxx:-defaultValue}`

##### sed 是一个专门用于查找并修改某一个文件的命令

- 它是针对某一行进行处理的,用得比较多得一个替换命令就是替换命令`sed "s/xx/xxx/"`
- 其中斜杠可以使用`#`进行替换，而使用正则匹配的东西可以通过`&`进行标记
- 使用sed可以修改多个配置文件`sed -i 's/xxx/xxx/'xx xx `

##### awk是一个专门用于统计、分割字段的命令

##### 获取shell脚本当前的执行路径`$(cd $(dirname ${0});pwd)`

- 其中$()和'``'表示的是同一个意思，都是先执行脚本然后再执行命令





