#### HTML DOM 对象 - 方法和属性

```javascript
# 一些常用的 HTML DOM 方法：
getElementById(id) - 获取带有指定 id 的节点（元素）
appendChild(node) - 插入新的子节点（元素）
removeChild(node) - 删除子节点（元素）从页面上拿掉节点，内存的节点还存在，
node.remove() - 也是删除子节点(元素),不单止在页面上拿掉节点，还把内存中的节点也干掉

# 一些常用的 HTML DOM 属性：
innerText  - 节点（元素）的文本值
innerHTML  - 节点 (元素) 的元素值
parentNode - 节点（元素）的父节点
childNodes - 节点（元素）的子节点
attributes - 节点（元素）的属性节点
```



| 方法                      | 描述                                                         |
| :------------------------ | :----------------------------------------------------------- |
| getElementById()          | 返回带有指定 ID 的元素。                                     |
| getElementsByTagName()    | 返回包含带有指定标签名称的所有元素的节点列表（集合/节点数组）。 |
| getElementsByClassName()  | 返回包含带有指定类名的所有元素的节点列表。                   |
| appendChild()             | 把新的子节点添加到指定节点。（包含增加和剪切两个操作） 该方法在Node.prototype里面 |
| c.insertBefore(a,b)       | 父节点c下在节点b之前插入a。在node.prototype                  |
| removeChild()             | 删除子节点。在node.prototype里面。并不能释放内存空间         |
| c.remove()                | 元素自杀。                                                   |
| replaceChild(new, origin) | 替换子节点。                                                 |
| insertBefore()            | 在指定的子节点前面插入新的子节点。                           |
| createAttribute()         | 创建属性节点。                                               |
| createElement()           | 创建元素节点。                                               |
| createTextNode()          | 创建文本节点。                                               |
| getAttribute()            | 返回指定的属性值。                                           |
| setAttribute()            | 把指定属性设置或修改为指定的值。                             |
| innerText()/textContent   | 把对应的标签转换为字符实体                                   |
| createComment()           | 增加注释节点                                                 |
| dataset                   | 自定义属性,data-name="xxx",data-age="66",存放在dataset的对象里面。可以通过getAttribute和setAttribute。兼容性，IE9以下均不兼容 |



#### 复制选中的文本

```javascript
methods: {
    CopyUrl(){
        let url = document.querySelector('#copyObj');
        url.select(); // 选择对象
        document.execCommand("Copy");
    },
}

# 注意事项
1、input 不可以添加 disabled 属性；
2、input的 width 和 height 不能为0；
3、input框不能有hidden属性；
4、input框必须必须挂到页面上才能被select，放在内存也不行 // 这句话的意思是将创建的元素放在document.createDocumentFragment创建的片段里面也不行
但是可以设置opacity。
```



#### 获取元素的时候要注意

```html
<form id="myForm">
    <input id="id">
</form>
<script>
	let form = document.getElementsByTagName('form')[0];
    console.log(form.id); // input，这就是基础篇章里面说的为什么不要命名那些W3C已存在的attribute原因
</script>
```



#### 绑定事件

```javascript
如果给DOM绑定元素，那么有三种方法
 - 内联事件，直接将onclick写在DOM上面 // 最早开始使用 DOM0层面
 - 在JS里面写DOM.onclick事件执行函数 // 有覆盖嫌疑 DOM0层面
 - 给DOM添加事件监听器 DOM.addEventListener(EventName, fn, Boolean) // 目前最佳，第二个参数是相同引用只用执行一次 DOM2
```



#### Event propagation

```javascript
During event propagation ,there are 3 phase: capturing, target, and bubbling. By default, event handlers executed in the bubbling phase(unless you set use capture to true).It goes from the deepest nested element outwards.Event.target was which DOM clicked;

# prevent bubbling
e.stopPropagation()、e.cancelBubble()
w3c regular: e
IE8 : window.event;
so : let e = e || window.event

# prevent default event
inline Event: direct return false;
w3c: e.preventDefault()
ie9: e.returnValue = false
```



#### EventFlow

```javascript
事件流: 描述从页面中接受事件的顺序 冒泡、捕获、目标
	- IE 提出事件冒泡流(Event Bubbling)
	- Netscape 事件捕获流(Event Capturing)
```



#### Event Loop

```javascript
Stack Event // Hight priority
Task Queue
	- macro [setTimeout、setInterval] // Lowest priority
	- micro [new promise、new Mutation observer] // Low priority
这里简单提一下事件循环的优先级以及在开发过程中遇到它们的地方，详情会在ECMAScript基础篇章进行剖析，这里有个大概印象即可
```
