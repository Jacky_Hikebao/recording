> ECMAScript 位于 瑞士的日内瓦(中立国家)，负责指定行业规范和标准，其中ECMA-262是脚本语言的规范，ECMAScript



#### 语言类型

````javascript
# 编译型语言
源码 > 编译器 > 机器语言 > 可执行的文件
优点：速度快

# 解释型语言
源码 > 解释器 > 解释一行执行一行
优点：不需要根据不同的系统平台进行移植，一致性

# 脚本语言
前端JavaScript（解释器在浏览器上）、后端PHP（解释器在服务器上面），所以可以在浏览器上看见JavaScript的源码，而看不到PHP代码
````



#### JavaScript的特点

```javascript
# JavaScript属于动态语言、解释型语言、弱类型语言
Ⅰ. 面向对象的编程语言
Ⅱ. 弱类型语言(无类型语言)
Ⅲ. 使用词法作用域
Ⅳ. 基于原型的继承机制
而C或者C++属于静态语言、编译型语言、强类型语言
```



#### 知识点梳理

```javascript
# 类型转换
    运算符 +、-、*、/、%、+=、-=、*=、/=、!、!!、>、<、==、!=
    所有数据类型 vs String => String vs String
    三大包装类：Number、String、Boolean
    typeof: 返回值是String、永远不会出现报错，括号可有可无
    falsty: ''、 ' '、0、false、undefined、null、NaN
    ParseInt: parseInt(target, y); // target具体目标、y是进制数,截取数字部分，没有返回NaN
	eval函数里面虽然存放的是字符串，但是执行的时候是表达式
    
# this指向
	谁调用指向谁
    箭头函数this指向上一级
    严格模式函数内this指向undefined、非严格模式this指向window
    new constructor 的时候，this指向实例化的对象

# 块级作用域
	IIFE
    let、const
    delete操作符对var、let、const变量进行操作的返回值是false
    
# Set、Map
    Set 对象允许你存储任何类型的唯一值，无论是原始值或者是对象引用。没有get方法
    Map 对象保存键值对。任何值(对象或者原始值) 都可以作为一个键或一个值。有get方法获取Map里面对应的值
    两者都可以通过遍历
    属于iterator Object的有String、Array、Map、Set
 
# Object.definedProperty
	使用该对象方法定义属性的时候，默认的值是enumerable、configurable、writable均为false
    
# setTimeout/setInterval
	返回的timeoutID是一个正整数值，用于标识调用创建的计时器setTimeout(); 可以传递此值clearTimeout()以取消超时。
    
# 表达式是取计算后的结果

# 模板字符串
function getPersonInfo(one, two, three) {
  console.log(one)
  console.log(two)
  console.log(three)
}

const person = 'Lydia'
const age = 21

getPersonInfo`${person} is ${age} years old`
如果使用标记模板字面量，第一个参数的值总是包含字符串的数组。其余的参数获取的是传递的表达式的值！

# String.raw
String.raw函数是用来获取一个模板字符串的原始字符串的，它返回一个字符串，其中忽略了转义符（\n，\v，\t等）。但反斜杠可能造成问题，因为你可能会遇到下面这种类似情况：

const path = `C:\Documents\Projects\table.html`
String.raw`${path}`
这将导致：

"C:DocumentsProjects able.html"

直接使用String.raw

String.raw`C:\Documents\Projects\table.html`
它会忽略转义字符并打印：C:\Documents\Projects\table.html

上述情况，字符串是Hello\nworld被打印出。

# Lambda
箭头函数没有prototype

# Event Loop
以下事件属于宏任务：
setInterval()
setTimeout()

以下事件属于微任务
new Promise()
new MutaionObserver()

当前执行栈执行完毕时会立刻先处理所有微任务队列中的事件，然后再去宏任务队列中取出一个事件。同一次事件循环中，微任务永远在宏任务之前执行。

```

