> 在编程的时候，不能期望所有功能都是现成的，因此你可能逐个编写计算机需要执行的代码和步骤，而忽略了这些步骤之上的抽象概念。在编程时，注意你的抽象级别什么时候过低时一项非常有用的技能。

#### 创建对象的方式

```javascript
1. 字面量
2. 系统自带的构造函数 new Object()
3. 自定义的构造函数
4. Object.create(null)
```



#### 对象属性的种类

```javascript
- 自有属性 
  - 数据属性
  - 存取器属性

- 继承属性
  - 数据属性
  - 存取器属性

存取器属性的描述符对象则用`get`属性和`set`属性代替`value`和`writable`。

数据属性的描述符对象的属性有`value`、`writable`、`enumerable`、`configurable`。

除了包含属性之外，每个对象还拥有三个相关的对象特性。分别为`prototype`、`class`、`extensible attribute`
```



#### 对象的`自闭`属性

```javascript
# 三大自有函数
`Object.preventExtensions()`、`Object.seal()`、`Object.freeze`.从左往右从左往右逐渐严格.

# enumerable
当`enumerable`为`false`时，表示某些操作会忽略当前属性。目前有四个操作会忽略`enumerable`为`false`的属性。
Ⅰ. `for...in`		    `inherit & ownproperty` 这里遍历出的继承属性是人为加上去的
Ⅱ. `Object.keys`	    `ownproperty`
Ⅲ. `JSON.stringify`  	`ownproperty`
Ⅳ. `Object.assign`    	`ownproperty`

特别地：`Reflect.ownkeys` 返回的key，包括`symbol`，且不受`enumerable`的影响。
在这里提醒一下`Object.defineProperty(obj, 'age' ,{value:22})` 里面的age的enumerable默认为false
```



#### 为什么使用Object.create(null)来创建对象？

```javascript
因为简单的对象时从`Object.prototype`派生的，所以它看起来就像拥有这个属性。因此，使用简单对象作为映射是危险的.

想要获得继承属性的特性，需要遍历原型链`Object.getPrototypeOf()`。如要设置属性的特性，可以使用`Object.defineProperty`、`Object.defineProperties`。在这里不得不提一点，对象的构造函数是用来初始化实例的，对象的原型属性是用于继承的。
```



#### 内存泄漏诱因之一

```javascript
let a = {
    p: {
        x: 1
    }
}
let b = a.p;
delete a.p;
执行这段代码之后，b.x的值依然是1。由于已经删除的属性的引用依然存在，因此在JS的某些视线中，可能因为这种不严谨的代码造成内存泄漏。所以在销毁对象的时候，要遍历属性中的属性，依次删除。
```



#### 使用`delete`操作符的时候，成功或没有任何副作用时返回 `true`

```javascript
且不能删除那些`configurable`为`false`的属性（尽管可以删除不可扩展对象的可配置属性），某些内置对象的属性时不可配置的，比如在全局作用域通过变量声明和函数声明创建的全局对象的属性

在BOM Ⅰ里面有更详细的扩展
```



#### in`操作符可以区分存在和不存在的属性，但值为`undefined`的属性

```javascript
let obj = {
    x: undefined
};

obj.x !== undefined; // false
obj.y !== undefined; // false
"x" in obj; 		 // true
delete obj.x;		 // true
"x" in obj;			 // false
```



#### 设置属性失败的几种情况

```javascript
- O中的属性P时只读的（writable:false）
- O中的属性P时继承属性，且它是只读的
- O中不存在自由属性P，O没有使用`setter`方法继承属性P，并且O的可扩展性`extensible`为`false`
```



#### `instanceof`

```javascript
instanceof运算符用于测试构造函数的prototype属性是否出现在对象的原型链中的任何位置
而且要注意的是，prototype是一个引用类型，里面保存的是堆内存的地址，即使里面的属性发生了变化也不会监听到。这样可以解释以下现象
function Car() {}
function Person() {}
Car.prototype.constructor = Person;
let test1 = new Car;
test1 instanceof Car;  // true

tips: instanceof有其局限性，在BOM Ⅰ里面有详细分析
```



#### set和get

```javascript
let obj = {
    set temp (value) {
        this.temp = value; // RangeError: Maximum call stack size exceeded
    },
    get temp () {
		return this.temp; // RangeError: Maximum call stack size exceeded        
    }
}

# 正确写法
var p = {
    name:"chen",
    _age:18,
    get age(){
        return this._age;
    },
    set age(val) {
        if (val<0 || val> 100) {//如果年龄大于100就抛出错误
            throw new Error("invalid value")
        }else{
            this._age = val;
        }
    }
};

```



#### 封印对象

```javascript
Object.preventExtensions()方法让一个对象变的不可扩展，也就是永远不能再添加新的属性。
Object.seal()方法封闭一个对象，阻止添加新属性并将所有现有属性标记为不可配置。当前属性的值只要可写就可以改变
Object.freeze() 方法可以冻结一个对象。一个被冻结的对象再也不能被修改；冻结了一个对象则不能向这个对象添加新的属性，不能删除已有属性，不能修改该对象已有属性的可枚举性、可配置性、可写性，以及不能修改已有属性的值。此外，冻结一个对象后该对象的原型也不能被修改。freeze() 返回和传入的参数相同的对象。

包括其原型链
```
