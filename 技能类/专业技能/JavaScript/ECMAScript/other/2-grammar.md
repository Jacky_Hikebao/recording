#### `import`与`require`

```javascript
import与require在ES6里面都是引入模块时使用，而且引入的模块时只读的，不能修改引入的模块。
而且`import`命令式编译阶段执行的，有点类似函数声明和var关键字声明变量，在代码运行之前。因此这意味着被导入的模块会先运行，而导入模块的文件会后执行。
```



#### delete

```javascript
通过var、const、let关键字声明的变量无法使用delete操作符删除
```



#### 解构赋值

```javascript
let arr = [1,2,3];
let [y] = [arr];
y === arr; // true，证明解构赋值是另一种赋值方式
let [x] = arr;
console.log(x); // 1
```



#### How many type does JavaScript have

```javascript
JavaScript only has primitive type and objects;
```



#### 打印引用类型的数据

```javascript
除了 null 和 undefined ，所有的基本类型和引用类型的构造函数原型上都会挂在到一个toString()的方法函数。对于引用类型的数据，可以将其序列化。对于基本类型，可以将其进行格式上面的改变。

最重要一点就是，如果要将引用类型的数据打印在页面上，必须先将其转换为基本类型。
```

