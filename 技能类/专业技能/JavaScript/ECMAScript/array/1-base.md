#### 数组一些行为不同于对象

```javascript
Ⅰ. 自动更新`length`
Ⅱ. 当设置`length`值比较小，会截断数组
Ⅲ. 类属性是`Array`
Ⅳ. 从`Array.prototype`继承方法
```



#### 如何创建一个0-100的数组

```javascript
Ⅰ. Object.keys([...Array(100)])
Ⅱ. Array.from({ length: 100 }, function () { return this.value++; }, { value: 0 })
```



#### 数组长度

```javascript
JavaScript中，数组的长度是32位，最大的索引值是 2^32-2。具有两个特殊行为。

length属性的值永远比最大索引多1
length小于当前长度时，索引值大于或等于n的元素将从中删除。
特别地：数组是对象的特殊形式，用数字索引来访问数组元素一般来说比访问常规的对象属性要快很多，同样地这意味着数组没有“越界”错误的概念。
```



#### 数组转化为字符串

```javascript
数组转化为`String`类型除了`toString`外，还有`join('')`
还有JSON.stringify()
```



#### 创建数组的方法

```javascript
let arr = [,1,23,4,,,]; // 不报错，最后一个逗号忽略
let arr1 = new Array(1,2,3,4,5,); // 报错
```



#### 类数组的原理

```javascript
let objArr = {
    '2': 3,
    '3': 4,
    length: 2,
    push: Array.prototype.push
}

objArr.push(1);
objArr.push(2);

输出: {
    '2': 1,
    '3': 2,
    length: 4,
    push: Array.prototype.push
}

过程： 
objArr.push(1) -> objArr[objArr.length++] = 1; // 注意了，这里是length++,不是++length
objArr.push(2) -> objArr[objArr.length++] = 2; 
```

