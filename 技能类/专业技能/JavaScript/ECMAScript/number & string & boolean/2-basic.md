#### padStart、String.raw

```javascript
使用`string.padStart`方法，我们可以在字符串的开头添加填充。传递的参数大于字符串长度会在字符串前面增加一个空格，如果小于则什么事情没有发生

String.raw函数式用来获取一个模板字符串的原始字符串的，它返回一个字符串，其中忽略了转移符(\n，\v,\t)
let str = 'C:\Users\lenovo\Desktop'
console.log(String.raw`${str}`);  // "C:UserslenovoDesktop"
正常应该是 C:\Users\lenovo\Desktop
```



#### symbol

```javascript
var str = Symbol('age');
let obj = {};
obj.str = 'jacky'; // obj: { str: 'jacky' }

// 点运算后面总是字符串；同理，在对象内部，使用Symbol值定义属性时，Symbol值必须放在方括号中；
```

