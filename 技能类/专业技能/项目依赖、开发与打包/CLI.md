#### step one

```javascript
chalk
	- chalk.green('string')

commander --program = require('commander')
	- command('string')
	- description('')
	- option('')
	- action('')
	- parse(process.argv)
	- program.outputHelp()

# get the pacakge version
const requiredVersion = require('../package.json').engines.node

# process.argv类似用户输入的命令，其中前两个参数分别是node和本地项目的执行位置
[ 'C:\\Program Files\\nodejs\\node.exe',
  'C:\\Users\\tin\\AppData\\Local\\Yarn\\Data\\link\\self\\bin\\self' ]

# 如何使项目命令可以全局运行？在任意地方就像项目里面一样？
A: 通过npm link| yarn link即可.比较有意思的就是，npm会把包放在npm的node_modules下面,yarn会把包放在link下面

# process.cwd()
ex: `C:\Users\tin\Desktop\self`
获取当前执行脚本的路径。有点类似shell里面的dirname

# action(param1, cmd)
第一个参数是command后面的第一个指令，cmd表示option里面的指令,有且只有两个指令同时存在,多出的存放在cmd的options里面。
.option('-d, --delete <path>', 'delete option from config')
上面第一个参数使用逗号隔开，真正的命令是使用第二个逗号里面的指令，所以要去除两个中横线才能作为cmd的key值，获取对应-d 后面的value值

```



#### create 创建APP程序

```javascript
# path
 ...paths <string> 路径或路径片段的序列。
返回: <string>
path.resolve() 方法将路径或路径片段的序列解析为绝对路径。

给定的路径序列从右到左进行处理，每个后续的 path 前置，直到构造出一个绝对路径。 例如，给定的路径片段序列：/foo、 /bar、 baz，调用 path.resolve('/foo', '/bar', 'baz') 将返回 /bar/baz。
如果在处理完所有给定的 path 片段之后还未生成绝对路径，则再加上当前工作目录。
生成的路径已规范化，并且除非将路径解析为根目录，否则将删除尾部斜杠。
零长度的 path 片段会被忽略。
如果没有传入 path 片段，则 path.resolve() 将返回当前工作目录的绝对路径。
```

