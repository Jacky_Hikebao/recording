#### gitlab搭建流程

```javascript
# 更新软件包 
yum update -y

# 安装 sshd
yum install -y curl policycoreutils-python openssh-server

# 启用并启动 sshd
systemctl enable sshd
systemctl start sshd

# 配置防火墙
打开 /etc/sysctl.conf 文件，在文件最后添加新的一行并按 Ctrl + S 保存：
net.ipv4.ip_forward = 1

# 启用并启动防火墙
systemctl enable firewalld
systemctl start firewalld

# 放通 HTTP
firewall-cmd --permanent --add-service=http

# 重启防火墙
systemctl reload firewalld

// tips：在实际使用中，可以使用 systemctl status firewalld 命令查看防火墙的状态。

# 安装 postfix
// GitLab 需要使用 postfix 来发送邮件。 
yum install -y postfix

#打开 /etc/postfix/main.cf 文件，在第 119 行附近找到 inet_protocols = all，将 all 改为 ipv4 并按 Ctrl + S 保存：
inet_protocols = ipv4

# 启用并启动 postfix：
systemctl enable postfix 
systemctl start postfix

# 配置 swap 交换分区
由于 GitLab 较为消耗资源，我们需要先创建交换分区，以降低物理内存的压力。
在实际生产环境中，如果服务器配置够高，则不必配置交换分区。
新建 2 GB 大小的交换分区：
dd if=/dev/zero of=/root/swapfile bs=1M count=2048

# 格式化为交换分区文件并启用：
mkswap /root/swapfile
swapon /root/swapfile

# 添加自启用。打开 /etc/fstab 文件，在文件最后添加新的一行并按 Ctrl + S 保存：
/root/swapfile swap swap defaults 0 0

# 安装 GitLab
将软件源修改为国内源
由于网络环境的原因，将 repo 源修改为`https://mirrors.tuna.tsinghua.edu.cn/gitlab-ce/yum/el$releasever/`

# 在 /etc/yum.repos.d 目录下新建 gitlab-ce.repo 文件并保存。内容如下：
[gitlab-ce]
name=Gitlab CE Repository
baseurl=https://mirrors.tuna.tsinghua.edu.cn/gitlab-ce/yum/el$releasever/
gpgcheck=0
enabled=1

# 安装 GitLab
刚才修改过了 yum 源，因此先重新生成缓存：（此步骤执行时间较长，一般需要 3~5 分钟左右，请耐心等待）
yum install -y gitlab-ce

# 初始化 GitLab
配置 GitLab 的域名（非必需）
打开 /etc/gitlab/gitlab.rb 文件，在第 13 行附近找到 external_url 'http://gitlab.example.com'，将单引号中的内容改为自己的域名（带上协议头，末尾无斜杠），并按 Ctrl + S 保存。
例如：external_url 'http://work.myteam.com'
记得将域名通过 A 记录解析到 <您的 CVM IP 地址> 哦。

# 使用如下命令初始化 GitLab：
（此步骤执行时间较长，一般需要 5~10 分钟左右，请耐心等待）
sudo gitlab-ctl reconfigure
```





## [Gitlab备份与恢复、迁移与升级](https://www.cnblogs.com/linkenpark/p/8893964.html)

 

## 0.Gitlab安装

1.安装和配置必要的依赖关系 
在CentOS7，下面的命令将在系统防火墙打开HTTP和SSH访问。

 

1. `yum install curl openssh-server postfix`
2. `systemctl enable sshd postfix`
3. `systemctl start sshd postfix`
4. `firewall-cmd --permanent --add-service=http`
5. `systemctl reload firewalld`

<https://mirror.tuna.tsinghua.edu.cn/help/gitlab-ce/>

 

版本：Gitlab Community Edition

 

**注意: gitlab-ce 镜像仅支持 x86-64 架构**

### Debian/Ubuntu 用户

首先信任 GitLab 的 GPG 公钥:

```
curl https://packages.gitlab.com/gpg.key 2> /dev/null | sudo apt-key add - &>/dev/null
```

再选择你的 Debian/Ubuntu 版本，文本框中内容写进 `/etc/apt/sources.list.d/gitlab-ce.list`

你的Debian/Ubuntu版本: 

 

```
deb http://mirrors.tuna.tsinghua.edu.cn/gitlab-ce/debian stretch main
```

安装 gitlab-ce:

```
sudo apt-get update
sudo apt-get install gitlab-ce
```

### RHEL/CentOS 用户

新建 `/etc/yum.repos.d/gitlab-ce.repo`，内容为

```
[gitlab-ce]
name=Gitlab CE Repository
baseurl=https://mirrors.tuna.tsinghua.edu.cn/gitlab-ce/yum/el$releasever/
gpgcheck=0
enabled=1
```

再执行

```
sudo yum makecache
sudo yum install gitlab-ce -y
```

安装后后，修改

/etc/gitlab/gitlab.rb 文件中external_url为你的域名，然后执行配置

gitlab-ctl reconfigure

 

启动脚本

```
cat /etc/systemd/system/gitlab.service
[Unit]``Description=gitlab` `[Service]``Type=oneshot``RemainAfterExit=yes``ExecStart=/bin/gitlab-ctl start``ExecStop=/bin/gitlab-ctl stop` `[Install]``WantedBy=multi-user.target
```

 

cat /usr/lib/systemd/system/gitlab-runsvdir.service

```
`[Unit]``Description=GitLab Runit supervision process``After=basic.target` `[Service]``ExecStart=/opt/gitlab/embedded/bin/runsvdir-start``Restart=always` `[Install]``WantedBy=basic.target`
```

 

3.配置并启动

 

1. `gitlab-ctl reconfigure`
2. `gitlab-ctl status`
3. `gitlab-ctl stop`
4. `gitlab-ctl start`

4.浏览到主机名和登录Browse to the hostname and login

 

1. `Username: root`
2. `Password: 5iveL!fe`

5.更多操作系统的安装方式，点击下方链接即可 
[CentOS6](https://about.gitlab.com/downloads/#centos6) 
[CentOS7](https://about.gitlab.com/downloads/#centos7) 
[Ubuntu14](https://about.gitlab.com/downloads/#ubuntu1404) 
[Ubuntu12](https://about.gitlab.com/downloads/#ubuntu1204)

 

## 1.Gitlab备份

使用Gitlab一键安装包安装Gitlab非常简单, 同样的备份恢复与迁移也非常简单. 使用一条命令即可创建完整的Gitlab备份:

 

1. `gitlab-rake gitlab:backup:create`

使用以上命令会在`/var/opt/gitlab/backups`目录下创建一个名称类似为`1481598919_gitlab_backup.tar`的压缩包, 这个压缩包就是Gitlab整个的完整部分, 其中开头的1481598919是备份创建的日期 
`/etc/gitlab/gitlab.rb` 配置文件须备份 
`/var/opt/gitlab/nginx/conf` nginx配置文件 
`/etc/postfix/main.cfpostfix` 邮件配置备份

 

### 1.1Gitlab备份目录

可以通过`/etc/gitlab/gitlab.rb`配置文件来修改默认存放备份文件的目录

 

1. `gitlab_rails['backup_path'] = "/var/opt/gitlab/backups"`

`/var/opt/gitlab/backups`修改为你想存放备份的目录即可, 修改完成之后使用`gitlab-ctl reconfigure`命令重载配置文件即可.

 

### 1.2Gitlab自动备份

实现每天凌晨2点进行一次自动备份:通过crontab使用备份命令实现

 

1. `0 2 * * * /opt/gitlab/bin/gitlab-rake gitlab:backup:create`

 

## 2.Gitlab恢复

Gitlab的从备份恢复也非常简单:

 

1. `# 停止相关数据连接服务`
2. `gitlab-ctl stop unicorn`
3. `gitlab-ctl stop sidekiq`
4. ``
5. `# 从1481598919编号备份中恢复`
6. `gitlab-rake gitlab:backup:restore BACKUP=1481598919`
7. ``
8. `# 启动Gitlab`
9. `sudo gitlab-ctl start`

 

## 3.gitlab迁移

要求：新服务器的gitlab版本与旧的服务器相同。

迁移如同备份与恢复的步骤一样, 只需要将老服务器`/var/opt/gitlab/backups`目录下的备份文件拷贝到新服务器上的`/var/opt/gitlab/backups`即可(如果你没修改过默认备份目录的话). 
但是需要注意的是新服务器上的Gitlab的版本必须与创建备份时的Gitlab版本号相同. 比如新服务器安装的是最新的7.60版本的Gitlab, 那么迁移之前, 最好将老服务器的Gitlab 升级为7.60在进行备份.

`/etc/gitlab/gitlab.rb` gitlab配置文件须迁移,迁移后需要调整数据存放目录 
`/var/opt/gitlab/nginx/conf` nginx配置文件目录须迁移

/etc/gitlab/gitlab-secrets.json # 复制新服务器相同的目录下

/etc/ssh/*key*  # 复制到新服务器相同目录下，解决ssh key认证不成功问题

1. `[root@linux-node1 ~]# gitlab-ctl stop unicorn`
2. `ok: down: unicorn: 0s, normally up`
3. `[root@linux-node1 ~]# gitlab-ctl stop sidekiq`
4. `ok: down: sidekiq: 0s, normally up`
5. `[root@linux-node1 ~]# chmod 777 /var/opt/gitlab/backups/1481598919_gitlab_backup.tar  # 或 chown git:git /var/opt/gitlab/backups/1481598919_gitlab_backup.tar`
6. `[root@linux-node1 ~]# gitlab-rake gitlab:backup:restore BACKUP=1481598919`

 

## 4.gitlab升级

1.关闭gitlab服务

 

1. `gitlab-ctl stop unicorn`
2. `gitlab-ctl stop sidekiq`
3. `gitlab-ctl stop nginx`

2.备份gitlab

 

1. `gitlab-rake gitlab:backup:create`

3.下载gitlab的RPM包并进行升级

 

1. `curl -s https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.rpm.sh | sudo bash`
2. `yum update gitlab-ce`
3. `或者直接安装高版本`
4. `yum install gitlab-ce-8.12.13-ce.0.el7.x86_64`
5. ``
6. `或者上官网下载最新版本 gitlab对应软件包 [gitlab官网](https://packages.gitlab.com/gitlab/gitlab-ce/packages/el/7/gitlab-ce-8.12.13-ce.0.el7.x86_64.rpm)`
7. `使用 rpm -Uvh gitlab-ce-8.12.13-ce.0.el7.x86_64`
8. ``
9. ``
10. `报错.`
11. `Error executing action `run` on resource 'ruby_block[directory resource: /var/opt/gitlab/git-data/repositories]'`
12. ``
13. `解决方法:`
14. `sudo chmod 2770 /var/opt/gitlab/git-data/repositories`

4.启动并查看gitlab版本信息

 

1. `gitlab-ctl reconfigure`
2. `gitlab-ctl restart`
3. `# head -1 /opt/gitlab/version-manifest.txt`
4. `gitlab-ce 8.7.3`

 

## 5.gitlab更改默认Nginx

更换gitlab自带Nginx，使用自行编译Nginx来管理gitlab服务。

编辑gitlab配置文件禁用自带Nignx服务器

 

1. `vi /etc/gitlab/gitlab.rb`
2. `...`
3. `#设置nginx为false,关闭自带Nginx`
4. `nginx['enable'] = false`
5. `...`

检查默认nginx配置文件，并迁移至新Nginx服务

 

1. `/var/opt/gitlab/nginx/conf/nginx.conf #nginx配置文件,包含gitlab-http.conf文件`
2. `/var/opt/gitlab/nginx/conf/gitlab-http.conf #gitlab核心nginx配置文件`

重启 nginx、gitlab服务

 

1. `$ sudo gitlab-ctl reconfigure`
2. `$ sudo service nginx restart`

访问报502。原因是nginx用户无法访问gitlab用户的socket文件。 重启gitlab需要重新授权

 

1. 1. `chmod -R o+x /var/opt/gitlab/gitlab-rails`

