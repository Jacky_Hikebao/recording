#### mode报错

```javascript
The 'mode' option has not been set, webpack will fallback to 'production' for this value. Set 'mode' option to 'development' or 'production' to enable defaults for each environment.

# 问题原因
由于webpack在打包的时候需要区分环境，所以需要一个模式来标识。这个表示一般系统在未配置的时候会发出警告并设置默认值为development。

# 解决方案
方案一：在package.json里面的scripts字段添加里面 --mode=development
"build": "webpack --mode=development"

方案二：在webpack.config.js里面添加一个mode字段
module.exports = {
	mode: 'development',
	entry: '***',
	output: {
		filename: '****'
	}
}
```



#### babel 报错

```javascript
- Maybe you meant to use
"plugins": [
  ["@babel/plugin-transform-runtime", {
  "corejs": 3
}]
]

# 问题原因
错误将corejs:3归纳为plugins的子项目，其实应该是和@babel/plugin-transform-runtime同级

# Tips
使用babel的时候会用到一大堆依赖
@babel/core
@babel/plugin-transform-runtime
@babel/preset-env
@babel/runtime
@babel/runtime-corejs3

# babel完整配置
  module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        "presets": ["@babel/preset-env"],
                        "plugins": [
                            ["@babel/plugin-transform-runtime", {
                                "corejs": 3
                            }]
                        ]
                    }
                }
            }]
    }

# 推荐版本
在webpack.config.js里面写
 module: {
        rules: [
            {
                test: /\.js$/,
                use: {
                    loader: "babel-loader"
                },
                 exclude: /node_modules/
            }]
    }
然后新建一个.babelrc目录负责维护babel的配置
{
    "presets": ["@babel/preset-env"],
        "plugins": [
            ["@babel/plugin-transform-runtime", {
                "corejs": 3
            }]
        ]
}
```



#### html-webpack-plugin 使用

```javascript
const HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports = {
    plugins: [
        new HtmlWebpackPlugin({
            template: './index.html', // 获取模板的位置
            filename: 'index.html' // 类似output里面的filename
        })
    ]
}

# 进阶config
`创建一个htmlConfig.js配置文件，然后再里面写入需要开启的变量`
mudule.exports = {
    dev: {
        template: {
            title: 'hello',
            header: false,
            footer: false
        }
    },
    build: {
        template: {
            title: '大家好',
            header: true
        }
    }
}
`在webpack.config.js里面引入htmlConfig.js`
const isDev = process.env.NODE_ENV === 'development';
const config = require('./htmlConfig.js')[isDev ? 'dev' : 'build'];
module.exports = {
     plugins: [
        new HtmlWebpackPlugin({
            template: './index.html', // 获取模板的位置
            filename: 'index.html',   // 类似output里面的filename
            config: config.template
        })
    ]
}
`在html模板里面书写语句`
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><%= (htmlWebpackPlugin.options.config.title) %></title>
</head>
<body>
    <% if(htmlWebpackPlugin.options.config.header){ %>
    <header>我是头部</header>
    <% } %>
    <div id="app"></div>
    <% if(htmlWebpackPlugin.options.config.footer){ %>
    <footer>我是footer</footer>
    <% } %>
</body>
</html>
```



#### cross-env报错

```javascript
# 传递NODE_ENV
 - 可以通过set方式进行变量的设置 build: "set NODE_ENV=development" 在window下可以
 - 也可以通过cross-env方式 build: "cross-env NODE_ENV=development" 兼容linux和window
 
 # cross-env 不是内部或外部命令问题处理
 - 全局install cross-env
```



#### webpack-dev-server

```javascript
# 原来在webpack.config.js里面的devServer是搭配webpack-dev-server来用的
一般在开发环境使用，所以，里面一般会有那种是否自动打开浏览器的设置(oepn)
设置端口号，是否代理，host等等
```



#### 处理css

```javascript
style-loader: 动态创建style标签，将css插入到head中
css-loader：负责处理@import等语句
postcss-loader和autoprefixer：自动生成浏览器兼容性前缀
less-loader：负责处理编译.less文件，将其转为css

# 在webpack.config.js中的完整配置
module.exports = {
    module: {
        rules: [
              {
                test: /\.less$/,
                use: [
                    'style-loader', 'css-loader', {
                        loader: 'postcss-loader',
                        options: {
                            plugins () {
                                return [
                                    require('autoprefixer')({
                                        "overrideBrowserslist": [
                                            ">0.05%",
                                            "not dead"
                                        ]
                                    })
                                ]

                            }
                        }
                    },
                    'less-loader'
                ]
            }
        ]
    }
}
其中use字段的loader执行顺序是从右往左的

# 将部分配置写到package.json里面去
  "browserslist": [
    ">0.25%",
    "not dead"]
然后只要在postcss-loader那个地方引入autoprefixer就行
use: [
    'style-loader', 'css-loader',
    {
        loader: 'postcss-loader',
        options: {
            plugins: [require('autoprefixer')]
        }
    },
    'less-loader'
]
```

