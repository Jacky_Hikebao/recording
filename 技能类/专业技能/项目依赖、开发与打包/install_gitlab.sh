#!/bin/bash
# install git-lab
# v1.0 by hikebao 2019/9/28

# test the network is working
domain=http://mygitlab.com
ping -c1 www.baidu.com &>/dev/null
if [ $? -ne 0 ];then
	echo "connect: unreachable"
	exit
fi

# if exist yum
type -a yum
if [ $? -ne 0 ]
then
	echo "no exist yum"
	exit
fi



yum install -y curl policycoreutils-python openssh-server &&
systemctl enable sshd &&
systemctl start sshd &&
#echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf &&
systemctl enable firewalld &&
systemctl start firewalld &&
firewall-cmd --permanent --add-service=http &&
systemctl reload firewalld &&
yum install -y postfix &&
sed -ri '/^inet_protocols=/cinet_protocols=ipv4' /etc/postfix/main.cf &&
systemctl enable postfix &&
systemctl start postfix &&
#dd if=/dev/zero of=/root/swapfile bs=1M count=2048 &&
#mkswap /root/swapfile &&
#swapon /root/swapfile &&
#echo "/root/swapfile swap swap defaults 0 0" >> /etc/fstab &&
yum install -y gitlab-ce &&
sed -ri "/external_url=/cexternal_url=$domain" /etc/gitlab/gitlab.rb &&
gitlab-ctl reconfigure
