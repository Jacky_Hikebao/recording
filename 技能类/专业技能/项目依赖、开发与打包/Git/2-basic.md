#### 一号开发任务坑

```ini
2019年7月9日这天，当时有个紧急的bug需要从分支2.0.10切换到2.0.8，为了不再去拉代码，所以第一次使用了命令stash，当时出现了一个问题，就是提示说...rm: commond not found。原来stash命令底层应用到rm，而且当时我的git内置的rm不知道咋滴就没了，后来重装一次git后rm命令就回来了。然后git stash也成功了，结合git stash list，
git apply stashName一起使用超级棒
```



#### 二号不懂英文坑

```ini
git checkout master
Switched to branch 'master'
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)
  
explain: 当前`master`分支比远程的`origin/master`分支要超前1个提交。提示将本地的提交推送到远程分支。出现`origin/master`表示的是远程分支，`master`表示是本地分支
```



#### 三号长期存在坑

```ini
当master分支或者origin/master分支与feature分支同时有提交的时候，那么就很可能产生冲突。
git merge feature1
Auto-merging readme.txt
CONFLICT (content): Merge conflict in readme.txt
Automatic merge failed; fix conflicts and then commit the result.

explain：一般情况下，如果线上和本地的提交没有同时修改同一个文件的话，系统会自动合并。
如果产生冲突Git用<<<<<<<，=======，>>>>>>>标记出不同分支的内容

橙色部分是`origin/master`分支上面的，绿色部分是本地的
# <<<<<<< HEAD
# Creating a new branch is quick & simple.
# =======
: Creating a new branch is quick AND simple.
: >>>>>>> feature1'
```

![产生冲突原因](./img/conflict.png)

![成功处理冲突](./img/solution_conflict.png)



#### 四号提交冲突坑

```ini
// 由于远程分支已经存在而产生的错误 

! [rejected]  257-master-cp -> bug-34687-ccq (fetch first)
error: failed to push some refs to 'http://****.git'
hint: Updates were rejected because the remote contains work that you do
hint: not have locally. This is usually caused by another repository pushing
hint: to the same ref. You may want to first integrate the remote changes
hint: (e.g., 'git pull ...') before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.
```



#### 五号专业名词坑

```ini
Git 常见英文意思
hint: 提示
remote: 远程仓库
ref: 引用 reference的缩写
reflog： reference log 每一次你提交或改变分支，引用日志都会被更新。
integrate: 集成
```



#### 六号提交中断坑

```ini
fatal: Unable to create 'C:/Users/lenovo/Desktop/getfilename/fsdf/.git/index.lock': File exists.

Another git process seems to be running in this repository, e.g.an editor opened by 'git commit'. Please make sure all processesare terminated then try again. If it still fails, a git process may have crashed in this repository earlier:remove the file manually to continue.

通俗讲，就是我们在commit的时候，会自动生成一个index.lock文件，操作完成后，会自动删除。如果在commit过程中，产生了意外，比如手动退出了，电脑死机了，断网了等等，导致操作失败，没有自动删除index.lock文件，那么下次再commit的时候，系统不知道你的index.lock没删除，它会傻傻的再去创建index.lock文件，这时候，发现已经目录下已经有一个index.lock文件了，懵逼了，不知道咋处理了，所以抛错给你：
: 在确定要删除index.lock文件之前，要确定产生index.lock文件的程序已经不再运行了,不然会导致数据丢失或者冲突的危机
```



#### 七号回退版本坑

```ini
回退前先在主线上创建一个分支，git branch mainLineBranch。然后再使用git reset --hard HEAD ^ 回退上一次的版本，等对应的代码修改完毕后再使用git merge mainLineBranch恢复到最新版本的代码，很可能会有冲突。
```



#### 八号误删分支坑

```ini
使用git reflog查看上一次的哈希值，然后使用git show HashID查看修改的内容是否正确，然后使用git branch branchName hashID恢复指定的分支，前提是里面的reflog还是完好无损。
由于reflog是依赖于.git/logs/文件，如果logs文件没有的话，怎么办？可以使用git fsck --full指令匹配悬空的分支对象，然后老规矩使用git show hashID展示对应的分支内容
```



#### fork

```javascript
GitHub的Fork 是什么意思
# 现在有这样一种情形：
有一个叫做Joe的程序猿写了一个游戏程序，而你可能要去改进它。并且Joe将他的代码放在了GitHub仓库上。

# 下面是你要做的事情
fork并且更新GitHub仓库的图表演示 // 如下图

1. Fork他的仓库：这是GitHub操作，这个操作会复制Joe的仓库（包括文件，提交历史，issues，和其余一些东西）。复制后的仓库在你自己的GitHub帐号下。目前，你本地计算机对这个仓库没有任何操作。
2. Clone你的仓库：这是Git操作。使用该操作让你发送"请给我发一份我仓库的复制文件"的命令给GitHub。现在这个仓库就会存储在你本地计算机上。
3. 更新某些文件：现在，你可以在任何程序或者环境下更新仓库里的文件。
4. 提交你的更改：这是Git操作。使用该操作让你发送"记录我的更改"的命令至GitHub。此操作只在你的本地计算机上完成。
5. 将你的更改push到你的GitHub仓库：这是Git操作。使用该操作让你发送"这是我的修改"的信息给GitHub。Push操作不会自动完成，所以直到你做了push操作，GitHub才知道你的提交。
6. 给Joe发送一个pull request：如果你认为Joe会接受你的修改，你就可以给他发送一个pull request。这是GitHub操作，使用此操作可以帮助你和Joe交流你的修改，并且询问Joe是否愿意接受你的"pull request"，当然，接不接受完全取决于他自己。
7. 如果Joe接受了你的pull request，他将把那些修改拉到自己的仓库！
```

![fork](./img/fork.png)



