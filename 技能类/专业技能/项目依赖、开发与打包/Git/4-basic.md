#### 分支合并

```javascript
三方合并称为diverged，Git 会使用两个分支的末端所指的快照（ C4 和 C5 ）以及这两个分支的工作祖先（ C2 ），做一个简单的三方合并。
```



#### 分支推送

```javascript
如果并不想让远程仓库上的分支叫做 serverfix ，可以运行git push origin serverfix:awesomebranch 来将本地的 serverfix 分支推送到远程仓库上的awesomebranch 分支。

# details
这里要详细讲解一下git push origin A:B。
git push origin 表示要推送分支到名为origin的远程仓库
A:B表示的是要将本地的分支A推送到对应的远程分支B上面
```



#### 远程分支

```javascript
# 这就是为甚什么当初我删除不了本地的远程分支原因，这里可是大有学问
要特别注意的一点是当抓取到新的远程跟踪分支时，本地不会自动生成一份可编辑的副本（拷贝）。 换一句话说，这种情况下，不会有一个新的 serverfix 分支 - 只有一个不可以修改的 origin/serverfix 指针。
可以运行 git merge origin/remoteBranchName 将这些工作合并到当前所在的分支。 如果想要在自己的 serverfix 分支上工作，可以将其建立在远程跟踪分支之上：git checkout -b customBranchName origin/remoteBranchName
```



#### 重大公告

```javascript
git branch --set-upstream-to remoteName/remoteBranchName
其中--set-upstream-to 可以缩写为-u
git branch -u remoteName/remoteBranchName

当设置好跟踪分支后，可以通过 @{upstream} 或 @{u} 快捷方式来引用它。 所以在 master 分支时并且它正在跟踪 origin/master 时，如果愿意的话可以使用 git merge @{u} 来取代 git merge origin/master 。

git log --pretty=online可以缩减为git log --oneline
```



#### 重新理解git fetch的作用

```javascript
当使用git branch -vv 命令时会展示分支的关联情况.这个命令并没有连接服务器，它只会告诉你关于本地缓存的服务器数据。 如果想要统计最新的领先与落后数字，需要在运行此命令前抓取所有的远程仓库。 可以像这样做： $ git fetch --all;git branch -vv
```



#### 变基原则

```javascript
总的原则是，只对尚未推送或分享给别人的本地修改执行变基操作清理历史，从不对已推送至别处的提交执行变基操作，这样，你才能享受到两种方式带来的便利。
```



#### 删除某个版本最佳实践

```javascript
使用git cherry-pick deleteVersion即可,因为这样可以对照着删除，而不会影响其他已提交的版本
```



#### git reset 常见的参数配置

```javascript
git reset <commit-id>  # 默认就是-mixed参数。

git reset –mixed HEAD^  # 回退至上个版本，它将重置HEAD到另外一个commit,并且重置暂存区以便和HEAD相匹配，但是也到此为止。工作区不会被更改。

git reset –soft HEAD~3  # 回退至三个版本之前，只回退了commit的信息，暂存区和工作区与回退之前保持一致。如果还要提交，直接commit即可  

git reset –hard <commit-id>  # 彻底回退到指定commit-id的状态，暂存区和工作区也会变为指定commit-id版本的内容

注意了，这里的HEAD用来表示当前的工作区更为准确，因为分支可以有很多，但是，工作区只有一个，用什么可以唯一标志当前的工作区呢？很明显用HEAD 是最为恰当。
而且，你可能注意到了，无论是branchName、HEAD、和tag，他们都是唯一指向一个哈希值，而git对比其他VC，最大的特点就是使用键值数据库来存储当前的数据。
另外git的命令有点类似bash，完全可以自己手写一个git仓库出来。无论是命令提示还是参数，都与脚手架很类似。
```



#### 增加远程仓库

```javascript
# git remote add remoteName URL
```



#### 快速切换到上一个分支

```javascript
git checkout -
```



#### 关联远程分支

```sh
关联之后，`git branch -vv` 就可以展示关联的远程分支名了，同时推送到远程仓库直接：`git push`，不需要指定远程仓库了。
git branch -u origin/mybranch

或者在 push 时加上 `-u` 参数
git push origin/mybranch -u
```



#### 重命名分支

```sh
git branch -m <new-branch-name>
```

