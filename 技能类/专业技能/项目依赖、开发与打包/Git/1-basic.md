#### 配置文件

```javascript
# git三类配置文件
系统级 < 用户级 < 本地仓库级，优先级依次递增，文件名均是gitconfig

# 位置
系统级：C:\Program Files\Git\mingw64\etc
用户级：C:\Users\登陆的用户名\.gitconfig
本地仓库级：当前开发项目的.git文件下

一般配置默认账号和用户存放在在用户级的配置文件.gitconfig
系统级配置文件一般配置告警颜色、字体大小等
```



#### 项目常用命令

```javascript
# branch
`git branch -a`:查看所有本地的分支情况
`git branch -r`:查看所有远程仓库分支情况
`git branch -vv`:查看本地分支与远程仓库的关系
`git branch -D A`:删除A分支
`git branch --track A B`:将本地分支A关联到远程分支B,注意A不存在,所以会创建一个A
`git branch --set-upstream-to A `:将当前分支关联到远程分支A
`git branch recoverBranch <hashValue>`:恢复某一个分支

# remote
`git remote rename oldName newName`:给远程分支换个名字
`git remote rm B`:删除远程分支B
`git remote add origin URL`:将本地程序关联远程<b>空仓库</b>

# stash
`git stash`:保存当前工作栈，用于临时checkout其他分支
`git stash list`:查看当前分支的工作栈情况，为了获取上一次保存的工作栈值
`git stash apply stash@{n}`:恢复到指定栈
`git stash clear` 清除stash栈

# log
`git log --pretty=oneline`:每一条提交记录都在一行内显示
`git reflog`:查看日志，用于获取版本的哈希值或者查看提交记录

# other
`git status`:查看当前仓库状态，用于查看modifies了啥文件
`git add F`: 将文件F加入本地暂存区
`git commit -m 'remark'`:  将文件加入本地仓库，其中remark为注释
`git commit -am 'remark'`:git add 与git commit的结合体
`git commit push origin  B`: 将B分支的内容推送至远程仓库
`git commit -amend` 修改最近一次提交记录
`git fetch B`:获取远程仓库B分支的数据
`git merge B`:将B分支合并到当前分支，用于合并指定分支到当前分支。
`git pull B`:git fetch与git merge的结合体
`git clone -b <branch-name> --single-branch URL` 仅克隆仓库里面的某一个分支
`git config --global credential.helper store` 默认保存用户的信息，下次`push`或者`pull`不用用输入账号密码
`git push -u -f B`:强制把本地数据覆盖远程分支B

# 回滚
`git reset --hard HEAD^`:回退一次版本
`git reset --hard hashValue`: 回退到指定哈希值的版本，目标和其他提交一并回退，log回退到目标log
`git revert hashID`: 回退到目标提交，不影响其他提交，在原基础上生成新的log
`~<num> ^ ` 移动HEAD步数,其中~是可以一次回退多步，而^ 一次只能回退一步

相对引用最多的就是移动分支。可以直接使用 `-f` 选项让分支指向另一个提交。例如: git branch -f master HEAD~3
上面的命令会将 master 分支强制指向 HEAD 的第 3 级父提交。
```



#### 回滚详解

```javascript
# git reset --hard hashID
适用场景： 如果想恢复到之前某个提交的版本，且那个版本之后提交的版本我们都不要了，就可以用这种方法。

# git revert hashID
适用场景： 如果我们想撤销之前的某一版本，但是又想保留该目标版本后面的版本，记录下这整个版本变动流程，就可以用这种方法。

# git reset HEAD fileName
文件状态处于已经staged

# git checkout FileName/.
文件状态处于modifies

# git reset --hard origin/master
文件处于commited状态

# git clean -f FileName/.
```



#### 2019年9月7日

```javascript
# git branch --track A B   git branch --set-upstream-to B
问：同样是将本地分支关联到远程分支B，它们的区别是什么
答：git branch --track A B 前提是A分支不存在，系统会自动创建一个分支关联到远程分支B；而git branch --set-upstream-to B是当将前分支关联到远程分支B，前提是存在分支。
'公告：git branch --set-upstream-to 可以缩写为git branch -u'

# git仓库管理有多少种状态
 - untrack（未跟踪，一般是新创建的文件）
 - deleted (被删除)、modifies(被修改也称为not staged)
 - stage （已被暂存）
 - commited (已经存放到本地仓库)

# git reset --soft与git reset --hard 以及git reset --mixed(默认)的区别
答：git reset 默认就是--mixed模式，返回到指定的版本未被commit的状态即回退HEAD和index，对工作区不影响
git reset --hard 模式，返回到指定的版本且modifies的文件全部丢失，而且git reflog上面没有对应的返回信息.
reset有个问题，就是可以回退到某个版本，但是会遗留多余的提交，ex: 下面的 7ea179d
```

![](./img/reset.png)

#### 2020年10月24日

```javascript
# 当遇到一些文件没有被添加进来.gitignore的时候。希望git不跟踪，但是保留磁盘文件
- 当已经存放到暂存区了可以使用 git reset HEAD [filename | glob模式]
- 当已经提交了 可以使用git rm --cache [filename | glob模式] -r 将其从跟踪状态变为删除状态且保留磁盘文件，然后再git commit -m 'xxx'移除git的跟踪模式即可

# git checkout 其工作范畴有限
只能对Modified文件生效，使其回退至Unmodified状态。面对已经存放在暂存区的工作文件是起不了作用的。

# 直接rm -rf 文件和git rm 之间的区别
git rm 在rm -rf原基础上多加了一步将修改存放至缓存区的操作，其它都一样

# 日常生活中使用频率较多的操作
git status		--- 查看状态	
git checkout .   --- 快速清理没有add的文件
git commit -am ''  --- 快速提交（这个快捷面对还没被git跟踪的文件是无效的，必须是已经跟踪的文件才能生效）
git reset --hard  --- 用于同步远程分支，回退到无或者unmodified状态
git reset --soft  --- 用于将所有的提交放在一次性提交里面执行,回退到缓冲区状态
git log --oneline -xx --- 用于查看回退至哪条日志
git branch xxxx --- 新增个人分支，一般都会在版本分支上面直接进行开发，直到推送至远程的时候才会创建分支
git push origin developBranch:remoteBranch --- 用于将本地的修改推送到远程的remoteBranch分支上面去
git branch -a -- 查看当前存在哪些分支
git clean . -f --- 用于清理Untrack文件，因为前端会打出很多dist包
git stash  --- 保留现场
git stash pop --- 恢复现场

为什么最近开发很少使用
git branch -vv
git branch -u
git reset --mixed hashValue ---主要用于将已经cmmited的文件回退到untrack/modified的状态  
```

