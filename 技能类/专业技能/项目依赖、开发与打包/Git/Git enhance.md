#### Git基本原理总结

```javascript
Git 的核心部分是一个简单的键值对数据库（key-value data store）。其中保存着git的对象，其中最重要的是`blob`、`tree`和`commit`对象，blob对象实现了对文件内容的记录，tree对象实现了对文件名、文件目录结构的记录，commit对象实现了对版本提交时间、版本作者、版本序列、版本说明等附加信息的记录。这三类对象，完美实现了git的基础功能：对版本状态的记录。

Git引用是指向git对象hash键值的类似指针的文件。通过Git引用，我们可以更加方便的定位到某一版本的提交。Git分支、tags等功能都是基于Git引用实现的。

git仓库主要关注点：`index`(暂存区)，`objects`(目录)，`config`(用户信息)，`refs`(分支信息)，HEAD(指向当前分支)，
```



#### 内容对象（content object）

```javascript
- git init  用于创建一个空的git仓库，或重置一个已存在的git仓库
- git hash-object -w --stdin，用于向Git数据库中写入数据
  该命令可将任意数据保存于 .git 对象数据库，并返回相应的键值。

  -w 选项指示 hash-object 命令存储数据对象；若不指定此选项，则该命令仅返回对应的键值。 --stdin选项则指示该命令从标准输入读取内容；若不指定此选项，则须在命令尾部给出待存储文件的路径。 **注意，这里并没有创建新的文件便直接存进数据库**

# 将文件保存至git数据对象中
  - echo 'xxxx' > test.txt 
    git hash-object -w test.txt 

# 查看Git数据库中数据。其中，-t选项用于查看键值对应数据的类型，-p选项用于查看键值对应的数据内容
  - git cat-file -t/-p <hashValue>
    
# 恢复test.txt文件某个版本的内容
	git cat-file -p hashValue > test.txt
```



**树对象（tree object）**

> 如果不是外部引入树对象，那么在暂存区里面的对象都是blob对象。

- 创建树对象之前首先要通过暂存一些文件来创建一个暂存区。可以通过命令update-index为一个单独文件创建一个暂存区`

  - ```console
     git update-index --add --cacheinfo <mode> <hashValue> <fileName>`
    ```

- 创建好了暂存区就可以往暂存区里面存一个或多个数据对象

  - ```console
    git write-tree
    ```

- 也可以往暂存区里面存放一个或多个树对象

  - ```console
    git read-tree --prefix=bak <hashValue>
    ```

- 查看暂存区内容

  - ```console
    git ls-files --stage   
    ```
    
    

**提交对象（commit object）**

> 现在有N个树对象，分别代表了我们想要跟踪的不同项目快照。然而问题依旧：若想重用这些快照，你必须记住所有三个 `SHA-1` 哈希值。 并且，你也完全不知道是谁保存了这些快照，在什么时刻保存的，以及为什么保存这些快照。 而以上这些，正是提交对象（commit object）能为你保存的基本信息

- 创建一个提交对象

  
  
  ```ini
  echo 'first commit' | git commit-tree d8329f
  提交对象的格式很简单：它先指定一个顶层树对象，代表当前项目快照；然后是作者/提交者信息（依据你的 user.name 和 user.email 配置来设定，外加一个时间戳）；留空一行，最后是提交注释。
  ```

**Git引用**

> 我们可以借助类似于 `git log 1a410e` 这样的命令来浏览完整的提交历史，但为了能遍历那段历史从而找到所有相关对象，你仍须记住 `1a410e` 是最后一个提交。 我们需要一个文件来保存 `SHA-1` 值，并给文件起一个简单的名字，然后用这个名字指针来替代原始的 `SHA-1` 值。

- 更新某个引用

  - ```console
    git update-ref refs/heads/master 1a410efbd13591db07496601ebc7a059dd55cfe9
    ```

**HEAD引用（HEAD reference）**

> 现在的问题是，当你执行 `git branch (branchname)` 时，Git 如何知道最新提交的` SHA-1` 值呢？ 答案是 HEAD 文件。

- 查看`HEAD`引用的值

  - ```console
    cat .git/HEAD
    ```

- 设置 HEAD 引用的值

  - ```console
    // 如果branchName是x/y/z的形式，则会在heads文件夹
    // 下面创建文件名闻x>y下的z文件 这里解释了胡雷当初为
    // 什么会有斜杠的分支名
    git symbolic-ref HEAD refs/heads/<branchName>
    ```

**标签引用（tag reference）**

> 前文我们刚讨论过 Git 的三种主要对象类型，事实上还有第四种。 标签对象（tag object）非常类似于一个提交对象——它包含一个标签创建者信息、一个日期、一段注释信息，以及一个指针。 主要的区别在于，标签对象通常指向一个提交对象，而不是一个树对象。 它像是一个永不移动的分支引用——永远指向同一个提交对象，只不过给这个提交对象加上一个更友好的名字罢了。

- 创建一个轻量标签

  - ```console
    // 轻量标签 一般是用于临时的标签 ，轻量标签仅仅记录了
    // commit的信息
    git update-ref refs/tags/test <hashValue>
    ```

- 创建一个附注标签

  - ```console
    // 附注标签 记录的信息更为详细 它包含了创建标签的作者 // 创建日期 以及标签信息。一般建议创建附注标签。
    git tag -a <tagName> <hashValue> -m <tagComment>
    ```

**引用规格**

纵观全书，我们已经使用过一些诸如远程分支到本地引用的简单映射方式，但这种映射可以更复杂。 假设你添加了这样一个远程版本库：

```console
$ git remote add origin https://github.com/schacon/simplegit-progit
```

上述命令会在你的 `.git/config` 文件中添加一个小节，并在其中指定远程版本库的名称（`origin`）、URL 和一个用于获取操作的引用规格（refspec）：

```ini
[remote "origin"]
	url = https://github.com/schacon/simplegit-progit
	fetch = +refs/heads/*:refs/remotes/origin/*
```

引用规格的格式由一个可选的 `+` 号和紧随其后的 `<src>:<dst>` 组成，其中 `<src>` 是一个模式（pattern），代表远程版本库中的引用；`<dst>` 是那些远程引用在本地所对应的位置。 `+` 号告诉 Git 即使在不能快进的情况下也要（强制）更新引用。

默认情况下，引用规格由 `git remote add` 命令自动生成， Git 获取服务器中 `refs/heads/` 下面的所有引用，并将它写入到本地的 `refs/remotes/origin/` 中。 所以，如果服务器上有一个 `master` 分支，我们可以在本地通过下面这种方式来访问该分支上的提交记录：

```console
$ git log origin/master
$ git log remotes/origin/master
$ git log refs/remotes/origin/master
```

上面的三个命令作用相同，因为 Git 会把它们都扩展成 `refs/remotes/origin/master`。

如果想让 Git 每次只拉取远程的 `master` 分支，而不是所有分支，可以把（引用规格的）获取那一行修改为：

```
fetch = +refs/heads/master:refs/remotes/origin/master
```

这仅是针对该远程版本库的 `git fetch` 操作的默认引用规格。 如果有某些只希望被执行一次的操作，我们也可以在命令行指定引用规格。 若要将远程的 `master` 分支拉到本地的 `origin/mymaster` 分支，可以运行：

```console
$ git fetch origin master:refs/remotes/origin/mymaster
```

你也可以指定多个引用规格。 在命令行中，你可以按照如下的方式拉取多个分支：

```console
$ git fetch origin master:refs/remotes/origin/mymaster \
	 topic:refs/remotes/origin/topic
From git@github.com:schacon/simplegit
 ! [rejected]        master     -> origin/mymaster  (non fast forward)
 * [new branch]      topic      -> origin/topic
```

在这个例子中，对 `master` 分支的拉取操作被拒绝，因为它不是一个可以快进的引用。 我们可以通过在引用规格之前指定 `+` 号来覆盖该规则。

你也可以在配置文件中指定多个用于获取操作的引用规格。 如果想在每次获取时都包括 `master` 和 `experiment` 分支，添加如下两行：

```ini
[remote "origin"]
	url = https://github.com/schacon/simplegit-progit
	fetch = +refs/heads/master:refs/remotes/origin/master
	fetch = +refs/heads/experiment:refs/remotes/origin/experiment
```

我们不能在模式中使用部分通配符，所以像下面这样的引用规格是不合法的：

```
fetch = +refs/heads/qa*:refs/remotes/origin/qa*
```

但我们可以使用命名空间（或目录）来达到类似目的。 假设你有一个 QA 团队，他们推送了一系列分支，同时你只想要获取 `master` 和 QA 团队的所有分支而不关心其他任何分支，那么可以使用如下配置：

```ini
[remote "origin"]
	url = https://github.com/schacon/simplegit-progit
	fetch = +refs/heads/master:refs/remotes/origin/master
	fetch = +refs/heads/qa/*:refs/remotes/origin/qa/*
```

如果项目的工作流很复杂，有 QA 团队推送分支、开发人员推送分支、集成团队推送并且在远程分支上展开协作，你就可以像这样（在本地）为这些分支创建各自的命名空间，非常方便。

### 引用规格推送

像上面这样从远程版本库获取已在命名空间中的引用当然很棒，但 QA 团队最初应该如何将他们的分支放入远程的 `qa/` 命名空间呢？ 我们可以通过引用规格推送来完成这个任务。

如果 QA 团队想把他们的 `master` 分支推送到远程服务器的 `qa/master` 分支上，可以运行：

```console
$ git push origin master:refs/heads/qa/master
```

如果他们希望 Git 每次运行 `git push origin` 时都像上面这样推送，可以在他们的配置文件中添加一条 `push` 值：

```ini
[remote "origin"]
	url = https://github.com/schacon/simplegit-progit
	fetch = +refs/heads/*:refs/remotes/origin/*
	push = refs/heads/master:refs/heads/qa/master
```

正如刚才所指出的，这会让 `git push origin` 默认把本地 `master` 分支推送到远程 `qa/master` 分支。

### 删除引用

你还可以借助类似下面的命令通过引用规格从远程服务器上删除引用：

```console
$ git push origin :topic
```

因为引用规格（的格式）是 `<src>:<dst>`，所以上述命令把 `<src>` 留空，意味着把远程版本库的 `topic` 分支定义为空值，也就是删除它。

或者你可以使用更新的语法（自Git v1.7.0以后可用）：

```console
$ git push origin --delete topic
```



注意每当仓库有任何改动的时候。eg：commit、pull、reset，文件refs下面的heads对应的分支哈希值会改变到最新，所以，当使用git branch newBranch的时候，git可以通过HEAD指向的最新的哈希值进行更新最新值
