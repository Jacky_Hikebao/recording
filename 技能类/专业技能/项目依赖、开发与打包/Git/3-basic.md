#### Local Rollback

```javascript
# modifies
git checkout FileName

# untracked
git clean -f FileName | rm -rf FileName

# staged but not commit
git rm --cache FileName | git rm --cache .

# after commit
git reset --hard origin/master | git reset --hard HashId
// tips: reset can't replace by revert, otherwise will launch an error;
// Before use git revert HEAD@{number} or git revert HashId, you must be in revert target branch
// if not, you will get a error tips;

# A lot of files want to ignort
You can create a directory names .gitignore, and add url which folder do you want to ignore.
```



#### git command

```javascript
git branch -av // add option v,can show more information about lasted commit
# show remote detail
git remote -v
```



#### git reset --hard 

```javascript
# 缺省情况
git reset缺省为git reset --soft

# 二者区别：
git reset –-soft：回退到某个版本，只回退了commit的信息，不会恢复到index file一级。如果还要提交，直接commit即可
git reset -–hard：彻底回退到某个版本，本地的源码也会变为上一个版本的内容，撤销的commit中所包含的更改被冲掉
```



#### How recover local deleted file

```javascript
# staged
git reset HEAD FileName

# modifies
git checkout FileName
```



#### cherry-pick

```javascript
获取某分支上的某个提交到当前分支
eg: git cherry-pick C1 C2 获取C1 C2到当前分支currentBranch <- C1 <- C2
```



#### rebase

```javascript
rebase：from the word meaning change the base branch.So generally be use to which branch would you want to change the base.
ex: git rebase -i targetBase // -i means interactive,targetBase can be hashID or branchName.
```



#### tag

```javascript
tag just as a reference for which one want to as a version milepost indentification. 
git tag v1.0 // tiny label
git tag -a v1.0 -m 'description' // tagging label
git push origin v1.0 // push the tag to the remote repository
after commit: git tag -a v1.0 hashID
```



#### Basic Git

```javascript
将git add命令理解为“添加内容到下一次提交中”而不是“将一个文件添加到项目中”要更加合适。

根据文件可能存在的状态，可将Git划分为三个区：暂存区、工作区以及仓库区。而工作区只是仓库区里面的一个版本。

# 文件 .gitignore 的格式规范如下：
1. 所有空行或者以 ＃ 开头的行都会被 Git 忽略。
2. 可以使用标准的 glob 模式匹配。
3. 匹配模式可以以（ / ）开头防止递归。
4. 匹配模式可以以（ / ）结尾指定目录。
5. 要忽略指定模式以外的文件或目录，可以在模式前加上惊叹号（ ! ）取反。

# only ignore the TODO file in the current directory, not subdir/TODO
/build
# ignore all files in the build/ directory
build/
    
    
记住，在 Git 中任何 已提交的 东西几乎总是可以恢复的。 甚至那些被删除的分支中的提交或
使用 --amend 选项覆盖的提交也可以恢复。 然而，任何你未提交的东西丢失后很可能再也找不到了。

# git remote
如果你已经克隆了自己的仓库，那么至少应该能看到 origin - 这是Git 给你克隆的仓库服务器的默认名字：

# git remote add
# git remote rm 
# git remote show remoteName
它也同样地列出了哪些远程分支不在你的本地，哪些远程分支已经从服务器上移除了，还有当你执行 git pull 时哪些分支会自动合并。

# git remote rename A B  将仓库A命名为仓库B
如果使用git branch -vv查看的话，可以看见斜杠左侧第一个名字就是远程仓库的名字，后面的如果有斜杠或者其他一些标识符都可以认为是命名空间。而且要注意的是，是在当前仓库敲git branch的，所以第一个名字要么没有，要么就是一样。而也可以查看其他远程分支，你可以使用"git pull remoteName/branchName"合并到当前分支上

对了，这里的remote概念可以跟之前的"git branch --set-upstream-to"以及"git branch --track"联系起来

# git remote prune originName
表示本地分支和远程仓库保持一致，如果远程仓库已经删除了某分支，但是本地分支还存在，那么可以使用该命令来同步本地分支的状态，后面的originName是远程仓库的名字

# git别名
git config --global alias.co checkout

# git reset HEAD -- FileName // 恢复某个文件，注意有两个中横线
  git reset HEAD . // 从暂存区中恢复中恢复所有文件
  git rest HEAD URL/* 恢复某个路径下面的所有文件
```



#### Git Branch

```javascript
# 微信版本踩坑，在Git branch章节开始就说明了分支的作用
使用分支意味着你可以把你的工作从开发主线上分离开来，以免影响开发主线。

# master分支
Git 的 `master` 分支并不是一个特殊分支。 它就跟其它分支完全没有区别。 之所以几乎每一个仓库都有 master 分支，是因为 `git init` 命令默认创建它，并且大多数人都懒得去改动它。哈哈哈，这里跟远程仓库origin有异曲同工之妙

Git 的分支，其实本质上仅仅是指向提交对象的可变指针。
那么，Git 又是怎么知道当前在哪一个分支上呢？ 也很简单，它有一个名为 HEAD 的特殊指
针。 请注意它和许多其它版本控制系统（如 Subversion 或 CVS）里的 HEAD 概念完全不
同。 在 Git 中，它是一个指针，指向当前所在的本地分支（译注：将 HEAD 想象为当前分支的别名）。

# git checkout master
这条命令做了两件事。 一是使 HEAD 指回 master 分支，二是将工作目录恢复成 master 分
支所指向的快照内容。
由于 Git 的分支实质上仅是包含所指对象校验和（长度为 40 的 SHA-1 值字符串）的文件，
所以它的创建和销毁都异常高效。 创建一个新分支就像是往一个文件中写入 41 个字节（40
个字符和 1 个换行符），如此的简单能不快吗？
这与过去大多数版本控制系统形成了鲜明的对比，它们在创建分支时，将所有的项目文件都
复制一遍，并保存到一个特定的目录。 完成这样繁琐的过程通常需要好几秒钟，有时甚至需
要好几分钟。所需时间的长短，完全取决于项目的规模。而在 Git 中，任何规模的项目都能在
瞬间创建新分支。 同时，由于每次提交都会记录父对象，所以寻找恰当的合并基础（译注：
即共同祖先）也是同样的简单和高效。 这些高效的特性使得 Git 鼓励开发人员频繁地创建和
使用分支。
```

