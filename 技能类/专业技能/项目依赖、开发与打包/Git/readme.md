### **版本控制**

- 1.理解`Git`的核心原理、工作流程、和`SVN`的区别
- 2.熟练使用常规的`Git`命令、`git rebase`、`git stash`等进阶命令
- 3.可以快速解决`线上分支回滚`、`线上分支错误合并`等复杂问题
- 发布后分支锁死，不可更改
- 全自动流程发布，多版本并存
- 自建GitLab，(代码管理、权限管理、提交日志查询)