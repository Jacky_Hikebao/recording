## 目录

- [构建流程](#webpack构建流程)
- [loader&plugin](#`loader`和`plugin`不同之处)
- [关于配置](#关于配置)

#### `webpack`构建流程

```javascript
- 开始编译：从配置文件和shell语句中读到的参数合并初始化complier对象，加载所有配置的插件，执行run方法开始编译
- 编译模块，从入口文件出发，调用所有配置的loader对模块进行解析翻译，在找到该模块依赖的模块进行处理
- 输出资源，根据入口和模块之间的依赖关系，组装成一个个包含多个模块的chunk，在把每个chunk转换成一个单独的文件加载到输出列表
```



#### 什么是`bundle`,什么是`chunk`,什么是`module`

- bundle：用webpack打包出来的文件

- chunk：webpack在进行模块的依赖分析的时候，分割出来的代码块。

  >  require.ensure(['./a'], require => {
  >     // 这里是需要对引入的模块执行的操作
  >  });

- module：开发中的单个模块



#### `loader`和`plugin`不同之处

```javascript
# 区别
- loader是webpack拥有加载和解析非js文件的能力，它会告诉webpack在require或者import的语句中被解析为'.xxx'的路径时，在对其打包前，先使用'xxx-loader转换一下'；
- plugin可以扩展webpack的功能，使得webpack更加灵活。在 webpack 的事件流中执行对应的方法。

# 几个常见的loader
- file-loader：把文件输出到一个文件夹中，在代码中通过相对 URL 去引用输出的文件
- url-loader：和 file-loader 类似，但是能在文件很小的情况下以 base64 的方式把文件内容注入到代码中去
- source-map-loader：加载额外的 Source Map 文件，以方便断点调试（这个在webpack5之后就直接内置在配置里面soruceMap）
- image-loader：加载并且压缩图片文件
- babel-loader：把 ES6 转换成 ES5
- css-loader：加载 CSS，支持模块化、压缩、文件导入等特性
- style-loader：把 CSS 代码注入到 JavaScript 中，通过 DOM 操作去加载 CSS。
- eslint-loader：通过 ESLint 检查 JavaScript代码

# 几个常见的plugin
// 先引入const A = require(xxx)，然后new A()
- define-plugin：定义环境变量
- terser-webpack-plugin：通过TerserPlugin压缩ES6代码
- html-webpack-plugin 用于生成index.html于dist文件里面
- mini-css-extract-plugin：分离css文件至一个单文件里面
- clean-webpack-plugin：删除打包文件
- happypack：实现多线程加速编译
- Open-Browser-Plugin：打开浏览器 npm run dev
```



#### 关于配置

- 写在`webpack.config.js`会覆盖`package.json`里面的配置 以下简称config

- `entry` & `ouput`

  > entry和output，其中entry的值可以为一个对象(也可以为一个string类型)，key为输出的包名，value为具体资源的位置，output目前知道值为Object类型，定义输出的文件名是通过key(filename)，value则是`[name].js`来定义输入什么名字输出就是什么名字
  >
  > - module为第三项，主要用于匹配那些不是原生`js、css、html、img`，然后将其转化为`js、css、img、html`.在module里面有一个配置项，是rules(Array数据类型),用于配置各种资源。Ex：`{test: /\.css$/, use: ['style-loader', 'css-loader']}`，而且use里面可以为数组也可以为数组对象，有点类似rules。当其为数组对象，说明里面必定存在更多的可选项，具体上npm搜索。

- externals的出现是为了加速webpack的打包，抽离部分代码不要令其打包进webpack里面去。 ex. CDN引入的资源

  > // main.js
  > externals: {
  >     // require('data') is external and available
  >     //  on the global var data
  >     'data': 'data'
  >   }
  >
  > // index.html
  > let data = require('data');

  - 易错地方

```javascript
var devFlagPlugin = new webpack.DefinePlugin({
`__DEV__`: JSON.stringify(JSON.parse(process.env.DEBUG || 'false'))
});
这个在config文件里面声明，但是在main.js文件里面却可以取得到`__DEV__`的值，足以说明，webpack提供一个API，用于打包工具和main.js通信，不过变量DefinePlugin需要放在plugins数组里面不然获取不到值

# 考察点七
entry: {
   app: './main.js',
   vendor: ['jquery'],
 },
plugins: [
   new webpack.optimize.CommonsChunkPlugin({
     name: 'vendor',
     filename: 'vendor.js'
   })
 ]
//entry 新语法(对象),结合plugins里面的CommonsChunkPlugin

```



#### 封装一个npm包需要注意什么

1. 使用TS的话，需要注意babel配置
2. .gitignore、eslintrc.js配置、.editorconfig配置、jest.config.json配置、.yarnrc和.npmrc源配置，授权配置、License文件、Readme以及发布包的时候配置、提交代码规范的配置

