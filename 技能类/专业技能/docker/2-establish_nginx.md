#### establish nginx server

```javascript
# step one
docker search nginx

# step two
docker pull imageName

# step three
docker run imageID

# step four
docker exec -it containerID bash

// How to get the containerID ?
Just use command `docker container ls`
```



#### How to install node

```javascript
# step one
Your OS must have wget command, otherwise `yum install wget`

# step two 
Go to the nodejs official website to get the latest install package URL.

# step three
wget URL
```



#### How to save the container 

```javascript
# step first
export containerID > fileName
execute above command, you will make a file name which you custom

# step second
cat fileName | docker import - imageName:Version
Then, you will get a image name `imageName`

# step third 
`docker run -p localIp:dockerIP -dit /bash/bin`
```

