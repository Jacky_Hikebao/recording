

#### 响应式原理



```javascript
如何通过definedProperty将某个组件里面的东西关联起来呢？
let input1 = document.querySelector('#input1');
let input2 = document.querySelector('#input2');
let data = {};
input1.addEventListener('input', (data) => {
    input2.value = input1.value
})
input2.addEventListener('input', data=>{
    input1.value=input2.value;
})

Object.defineProperty(data, 'jack', {
    get () {
        return input1.value;
    },
    set (newValue) {

        input1.value = newValue;
        input2.value = newValue;
    },
    enumerable: true,
    configurable: true
});
```
