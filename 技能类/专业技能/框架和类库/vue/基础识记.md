#### 父子孙生命周期

```javascript
beforeCreated 				// 父组件
created					    // 父组件
beforeMount					// 父组件
	beforeCreated	   		 // 子组件 
    created					// 子组件
    beforeMount				// 子组件
    	 beforeCreated		 // 孙组件
    	 created  			// 孙组件
    	 beforeMount  		 // 孙组件
         mounted		  	 // 孙组件
    mounted					// 子组件
mounted						// 父组件

# 案例
<father>
    <son ref="sonRef"></son>
</father>

// 现在需要在父组件里面调用子组件的方法
created () {
    this.$refs.sonRef.featchData(); // 报错，提示错误。因为this.$refs.sonRef为undefined
}

mounted () {
    this.$refs.sonRef.featchData(); // 正常
}
这就是为什么不建议在created里面进行一些子组件操作得原因，而且希望在使用ref前最好判断是否存在该元素得判断。
es5写法：this.$refs.sonRef && this.$refs.sonRef.featchData();
ts写法：this.$refs.sonRef?.featchData();
```



#### 组件通信

```javascript
# 父子间通信
props和$emit  // 响应式
$parent和$children  // 获取对应的父和子
ref   // 获取DOM或者对应实例,
provide和inject // 静态，可以跨N层级
$attrs和$listeners // 省略在子级里面props声明，直接v-on='evenName'

# 跨级通信
Bus
Vuex
provide和inject 
$attrs和$listeners
url之params通信，也称之为路由通信
browser之storage通信
window通信
```



#### 组件 vs 原生

```javascript
# Vue组件都有什么
- 事件
- 数据（响应式）
- 插槽（具名插槽和作用域插槽）
- 属性（class、style、props）
- ref（指向组件|DOM的实例）

# 原生HTML有什么
- 完全的自由，原始时代，API丰富，随便使用，考验一个人的基础
- 事件
- 数据（非响应式）
- 伪插槽（iframe）
- 属性（这里的属性不可传递）
- 原生DOM的Ref，通过document.querySelector
```



#### **Vue之Css作用域**

- BEM: 代表块（Block），元素（Element），修饰符（Modifier）
- Scoped: 为每个Dom元素加上唯一hash值，样式传透
  - `less`使用 `/deep/`
  - `stylus`使用  >>>
  - `scss`   使用::deep
- modules：将样式保存至`$style`里面

#### **Vue小技巧**

1.当使用`v-if`时，模板渲染在浏览器会产生`<!---->`，但是使用`v-show`不会有。

2.当被内置组件`keep-alive`包含，子组件的`destroy`不会触发。

3.`ref`在组件上表示引用，`ref`在标签上表示`DOM`元素

4.父给子传数据`v-bind`,父给子传`DOM`结构`slot`。子给父传数据`emit、slot-scope`

