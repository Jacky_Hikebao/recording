#### css选择器

```javascript
1.元素选择器
语法：标签名{}
eg：p{} // 为所有的p元素设置样式。

2.ID选择器
语法：#id{} // id值唯一，不能重复
ed：#box{} // 为id为box的元素设置样式。

3.类选择器
语法：.class{}
eg：.box{} // 为所有的class值为box的元素设置样式。

4.分组选择器（也叫并集选择器）
语法：选择器1，选择器2，选择器N{}
eg: #box1,.box2,p{} // 为id为box1，class为box2和p的元素共同设置样式。

5.交集选择器
语法：选择器1选择器2选择器3{}
eg：p.box1{} 为class值为box1的p元素设置样式，注意id为唯一值，一般id选择器不会存在于交集选择器中（没必要）。

6.后代选择器
语法：选择器1 选择器2{}
eg：p .box{} // 选中指定祖先元素p的指定后代.box。

7.子元素选择器
语法：父元素>子元素{}
eg：p>.box{} // 选中制定父元素p的制定子元素。box。**注意与后代元素选择器的区别**

8.伪类选择器
- 伪类可以用来表示一些特殊的状态：
  ：link - 未访问过的超链接。
  ：visited - 已访问过的超链接。
  ：hover - 鼠标移入的元素。
  ：active - 正在点击的元素。
  由于选择器优先级的问题，给超链接a设置伪类时，需要注意他们的顺序，一般的顺序是：love hate 记作（爱与恨）,方便记忆
  :link > :visited > :hover > :active

9.兄弟选择器
 p + div：p后面的第一个div // 不受文本节点的影响，受其元素节点的影响
 p ~ div：p后面的所有div  // 不受文本节点和元素节点的影响

它们和子元素选择器的区别是：它们都是p与div是同级的，而子元素选择器是有嵌套关系的。

选择器最主要的功能就是跟对应的元素在时机恰当的时候添上对应的样式。那么日常开发过程中如何保证准确定位到那个所谓【对应的元素呢？】
BEM命名规范  -- 最常用
scoped     --原理就是在每一个Dom节点添加唯一哈希值，缺点就是解析慢，文件大
module     -- 将所有的样式放在一个对象$style里面管理,而且在使用前需要在css-loader配置里面启用modules: true。一般在style 标签里面加上module即可，如果要区分模块使用module='b'
```



## 选择器的优先级

为同一个元素设置多个样式时，此时哪个样式生效由选择器的优先级确定：

- 选择器的优先级（权重）：

|   ·    | 内联样式 | id选择器 | 类和伪类选择器 | 元素选择器 | 统配选择器 | 继承的样式 |
| :----: | :------: | :------: | :------------: | :--------: | :--------: | :--------: |
| 优先级 |   1000   |   100    |       10       |     1      |     0      |     无     |

- 当一个选择器中含有多个选择器时，需要将所有的选择器的优先级进行相加，然后再进行比较，优先级高的优先显示，选择器的计算不会超过其最大的数量级（10个id选择器的优先级不能达到1000）分组选择器（并集选择器）的优先级单独计算，不会相加。
- 样式后面加！important，该样式获取最高优先级，内联样式不能加！important属性。
- 样式相同的谁在下面执行谁（样式的覆盖）。



#### 伪元素与伪类

```javascript
伪元素 after、before、first-letter、first-line

1. 伪对象要配合content属性一起使用
2. 伪对象不会出现在DOM中，所以不能通过js来操作，仅仅是在 CSS 渲染层加入
3. 伪对象的特效通常要使用:hover伪类样式来激活

伪类 active、focus、hover、link、visited、:first-child、:lang

区别：其中伪类和伪元素的根本区别在于：它们是否创造了新的元素,, 这个新创造的元素就叫 "伪无素" 
伪元素/伪对象：不存在在DOM文档中，是虚拟的元素，是创建新元素。 这个新元素(伪元素) 是某个元素的子元素，这个子元素虽然在逻辑上存在，但却并不实际存在于文档树中. 伪类：存在DOM文档中，(无标签,找不到, 只有符合触发条件时才能看到 ), 逻辑上存在但在文档树中却无须标识的“幽灵”分类可以同时使用多个伪类，而只能同时使用一个伪元素；CSS3中伪类和伪元素的语法不同

使用场景：
- 清除浮动 (在父元素里面设置after伪元素，而且记住位元素要给content和display: block;)
- 面包屑、连接多个项目
    .jack:not(:last-child)::after
- 三角形 border:6px solid rgba(0,0,0,0.7);    # 注意，这里要加上绝对定位，不然会有问题
    border-color:transparent rgba(0,0,0,0.7) transparent transparent
- 阴影
	box-shadow搭配transform里面的rotate
- 字体图标
	iconfont或者awesome-font
```



#### css可继承的常用属性

```html
<style>
    body {
        你可以设置可继承的属性在body元素上，这样样式就可以被所有子代应用
    }
    .inherit {
	1、字体系列属性
        font-family：字体系列
        font-weight：字体的粗细
        font-size：字体的大小
        font-style：字体的风格
        
    2、文本系列属性
        text-indent：文本缩进
        text-align：文本水平对齐
        line-height：行高
        word-spacing：单词之间的间距
        letter-spacing：中文或者字母之间的间距
        text-transform：控制文本大小写（就是uppercase、lowercase、capitalize这三个）
        color：文本颜色
        
	4、列表布局属性：
        list-style: <list-style-type> | <list-style-position> | <list-style-image>
            
    5、光标属性：
		cursor：光标显示为何种形态
    }
  可使用inherif继承属性
</style>
```

