## CSS基本语法

- 选择器 + 声明块
  - 选择器 - 通过CSS选择器选中页面中的指定元素，下面会重点写。
  - 声明块 - 选择器后面跟着的是声明块，使用{}括起来，由一个个声明组成，声明由名值对组成，每一个声明使用；结尾。

- # at 规则

  ### **嵌套规则**

  - 所谓“嵌套规则”，就是带有花括号`{}`, 语法类似下面的规则：

    ```css
    @[KEYWORD] {
      /* 嵌套语句 */
    }
    ```

    

  - `@keyframes`：用来声明Css3 animation动画关键帧用的
  
     ```
     @keyframes fadeIn {
      0% {
          opacity: 0;
      }
      100% {
          opacity: 1;
      }
   }
     ```

    
  
  - `@media`：媒介查询，响应式宽度
  
       ```css
       @media all and (min-width: 1280px) {
           /* 宽度大于1280干嘛干嘛嘞... */ 
       }
       @media 
       (-webkit-min-device-pixel-ratio: 1.5), 
       (min-resolution: 2dppx) { 
           /* Retina屏幕干嘛干嘛嘞... */ 
       }
       @media print {
           /* 闪开闪开，我要打印啦！ */ 
       }
       @media \0screen\,screen\9 {
           /* IE7,IE8干嘛干嘛嘞... */ 
       }
     @media screen\9 {
           /* IE7干嘛干嘛嘞... */ 
     }
     ```
  
  - `@page`：这个规则用在打印文档时候修改一些CSS属性。使用`@page`我们只能改变部分CSS属性，例如间距属性`margin`, 打印相关的`orphans`, `widows`, 以及`page-break-*`, 其他CSS属性会被忽略。
  
     ```css
       @page :first {
         margin: 1in;
     }
  
     @page的伪类包括：:blank, :first, :left, :right
     ```
  
  - `@supports`：是否支持某CSS属性声明的AT规则
  
       ```css
     /* 检查是否支持CSS声明 */ 
       @supports (display: flex) {
         .module { display: flex; }
       }
  
       /* 检查多个条件 */ 
     @supports (display: flex) and (-webkit-appearance: checkbox) {
         .module { display: flex; }
     }
     ```
  
  - `@font-face`：这个大家可能比较熟，自定义字体用的。IE6也支持。
  
       ```css
       @font-face {
         font-family: 'MyWebFont';
       src:  url('myfont.woff2') format('woff2'),
               url('myfont.woff') format('woff');
     }
     ```
  
  - `@document`：CSS 4.0规范有相关说明。如果文档满足给定的一些条件，就可以应用我们指定的一些样式。比如说，这个CSS文件被子站A调用，和被子站C调用，我们可以通过域名匹配来执行不同的CSS样式。这样，我们可以有效避免冲突，或者防止外链之类。
  
    ```css
    :( 大部分浏览器不兼容
    @document url(http://www.w3.org/),
                url-prefix(http://www.w3.org/Style/),
                domain(mozilla.org),
                regexp("https:.*")
    {
    /* 该条CSS规则会应用在下面的网页:
         + URL为"http://www.w3.org/"的页面.
         + 任何URL以"http://www.w3.org/Style/"开头的网页
         + 任何主机名为"mozilla.org"或者主机名以".mozilla.org"结尾的网页     
         + 任何URL以"https:"开头的网页 */
    
    ```
  
  /* make the above-mentioned pages really ugly */
    body { color: purple; background: yellow; }
}
    ```
  
    
  
    ### **常规规则**

  - 所谓“常规规则”指的是语法类似下面的规则：
  
    ```css
  @[KEYWORD] (RULE);
    ```

  - `@charset`：定义字符集。字符设置据说会被HTTP头覆盖。某些软件，例如Dreamweaver新建CSS文件时候，自动会带有下面所示代码，但实际开发时候，作用不大，因为meta中已经有所设置(`<meta charset="utf-`)，会覆盖，所以我都是直接删掉的。

    ```css
   @charset "utf-8";
    ```
  
    

  - `@import`：导入其他CSS样式文件。实际上线时候，不建议使用，多请求，阻塞加载之类。但本地开发可以使用，用做CSS模块化开发，然后使用一些(如grunt)工具进行压缩并合并。但是呢，相比less, sass等还是有不足，就是`@import`语句只能在CSS文件顶部，使得文件的前后关系控制，就不那么灵活。
  
    
  
    ```css
  @import 'global.css';
    ```

  - `@namespace`：任何 @namespace 规则都必须在所有的 @charset 和 @import 规则之后, 并且在样式表中，位于其他任何 style declarations 之前。
  
       ```css
       @namespace url(http://www.w3.org/1999/xhtml);
     @namespace svg url(http://www.w3.org/2000/svg);
  
       /* 匹配所有的XHTML  元素, 因为 XHTML 是默认无前缀命名空间 */
     a {}
  
       /* 匹配所有的 SVG  元素 */
       svg|a {}
  
       /* 匹配 XHTML 和 SVG  元素 */
       *|a {}
     ```

