#### 【input的宽度】💗并不是给元素设置display:block就会自动填充父元素宽度。input 就是个例外，其默认宽度取决于size特性的值,如果要其填充满父元素，还要加上width:100%

#### 绝对定位和固定定位时，同时设置 left 和 right 等同于隐式地设置宽度

#### 相邻兄弟选择器之常用场景 ul>li + li 这样可以避免第一个li应用到对应的样式

#### 要使模态框背景透明，用rgba是一种简单方式 ，父级元素的opacity会影响子代元素，所以一般modal和mask是没有血缘关系

#### 【定宽高比】♥css实现定宽高比的原理：padding的百分比是相对于其包含块的宽度，而不是高度 

#### IE11下的flex: space-evenly 会有兼容性问题，最好使用space-around或者space-between

