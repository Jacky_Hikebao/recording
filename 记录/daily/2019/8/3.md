## 常用见的node命令

- **shelljs** ：ShellJS是 Node.js API之上的Unix shell 命令的可移植**（Windows / Linux / OS X）**实现；说白了就是使用javascript来模拟shell

- 首先，读取文件的时候，一个是路径的问题，通过对文件进行一次遍历，区分哪个是文件，哪个是目录，如果是目录，那么将对应的路径 / 转为 \
- 其次，如果要分割一个文件里面的内容，可以通过string.split('/n')进行分割，这里会忽略掉里面的空行

- 在node中，使用process.cwd()的时候，是以当前执行的文件作为标准，而不是声明语句的那个文件所在的位置为标注。

