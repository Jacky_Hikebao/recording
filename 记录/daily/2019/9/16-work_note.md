#### Vuex

```javascript
# step 1
 - 创建一个vuex文件夹并在底部新建一个index.js作为vuex主入口
 - export default new Vuex.Store({});
 - 引入模块，并在Store里面声明modules字段
 - 在修改处this.$store.commit | this.$store.dispatch 前者是mutations同步选项，后者是actions异步选项 // 注意mutations和actions后面都有s
 - 在使用处计算属性声明对应的变量并且this.$store.modulesName.variable
```



#### proxy

```javascript
# request.headers.referer
HTTP Referer是header的一部分，当浏览器向web服务器发送请求的时候，一般会带上Referer，告诉服务器该网页是从哪个页面链接过来的，服务器因此可以获得一些信息用于处理。
```

