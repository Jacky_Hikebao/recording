#### grasp skill except work need

```ini
score standard: 
1-3: basic
3-6: know principle
6-9: know how to create

# grasped
1. Git			3.5     : 6   // 一边工作一边学习 +
2. GitLab		4	    : 6   +
3. Shell		2	    : 6  // 一边工作一边学习 +
4. Docker		2		: 6  
5. Nginx		1		: 6   
6. Nodejs		1.5
7. Npm & Yarn	3
8. Webpack		2

9. JavaScript	 5    +
10. DOM			3     +
11. BOM			3     +
12. VUE			3.5
13. Echart		3
14. Component(Element-ui、Iview-ui、mint-ui、vux-ui)	3 // 一边工作一边学习
15. project experience	3

total: 42.5
average: 2.83

# unlearned
TypeScript
React
D three.js
Data structrue
Network protocol
Algorithm
Design pattern
Client safty
```



#### Git 3.5

```javascript
在知道Git拿了这么低的评分后，才知道，原来我只是个会用工具的家伙，从头到脚，对工具的由来和原理都不懂得的菜鸡。如果有人问我，Git是什么东西，这是干什么的，姑且我答不上一个所以然来。所以，期末要达到6分，还是有2.5分的差距。从以下几方面来提升Git的技能
	- Git由来，多点写一些博客，分析，说明Git的背景，竞品有什么，优势在哪里 +1
	- Git的原理是什么，基于什么创造出来，怎么去维护，怎么去提高开发效率，有什么好的提高性能的建议 +1
	- Git的PPT能力？？ +0.5
    
```



#### GitLab 4

```javascript
Git和GitLab可谓是唇亡齿寒的关系，如果说Git是一个本地代码管理工具，那么，GitLab就是一个线上的，团队化的工程，里面有一些wiki、issue、还有CI/CD，如何和commit搭配等等，以及配置打包的路径方案。主要是梳理使用方式，如何配合团队开发进行使用
	- tag、cicd、issue、permission  +2
```



#### Shell 2

```javascript
与linux交流的工具，一个合格的开发应该掌握的脚本语言。不要说职业限制了你的发展，这是一个基本的命令，了解当前进程，调度事件的工具
	- 基本语法 +2
	- 学习别人的开发经验 +1
	- 结合其他工具一起使用的，以及未来走向 +1
```



#### Docker 2

```javascript
Docker容器化这一概念近段时间异常之火爆，一方面有效提高资源的利用效率，另一方面能跨多端、稳定，集成速度快的特点
	- 基本命令 +2
	- 如何提升项目的开发速度 +2
```



#### Nginx 1

```javascript
作为高并发的代表，很多成熟的公司都在使用，不了解里面的工作原理实在是对不住
	- 如何代理	+0.5
	- 如何高并发 +1.5
	- 基本命令 +2
	- 如何提高开发效率 +1
```



#### Nodejs 1.5

```javascript
作为前端的服务器级别环境，用它来创造一些工具是必然的，里面可以糅合多种语言、shell、python，等等
	- 这里还未算点亮，需要学习
```



#### Npm & Yarn 3

```javascript
作为知名的包管理工具，他可以有效的协助开发者管理各种包，那么包是从何而来，又如何维护，里面涉及了什么算法和逻辑？
```



#### Webpack 2

```javascript
前端打包大佬，不懂其原理不能称为及格
```



#### JavaScript

```javascript
核心，promise、generator、regexp
```



#### DOM 与 BOM

```javascript
学习、BOM、DOM的核心
```



#### vue、component、echart

```javascript
原理与实践
```



#### project

```javascript
沟通与流程
```

